using Extensions;
using UnityEngine;
using UnityEngine.Playables;

namespace VRGame
{
	public class Entity : UpdateWhileEnabled, IDestructable
	{
		[HideInInspector]
		public float hp;
		public float Hp
		{
			get
			{
				return hp;
			}
			set
			{
				hp = value;
			}
		}
		public int maxHp;
		public int MaxHp
		{
			get
			{
				return maxHp;
			}
			set
			{
				maxHp = value;
			}
		}
		public Transform trs;
		public CharacterController controller;
		public float moveSpeed;
		public float slideRate;
		public float drag;
		public PlayableDirector movementPlayableDirector;
		public LayerMask whatICollideWith;
		public Transform groundCheckPoint;
		public float groundCheckDistance;
		public float moveAnimSpeed;
		[HideInInspector]
		public Vector3 move;
		[HideInInspector]
		public bool dead;
		[HideInInspector]
		public bool isGrounded;
		[HideInInspector]
		public Vector3 slideVelocity;
		[HideInInspector]
		public Vector3 pushVelocity;
		public float mass;
		public bool isFlying;

		public override void OnEnable ()
		{
			hp = maxHp;
			base.OnEnable ();
		}

		public override void DoUpdate ()
		{
			if (dead)
				return;
			isGrounded = controller.isGrounded;
			HandleGravity ();
			HandleDrag ();
			controller.Move((move + slideVelocity + pushVelocity) * Time.deltaTime);
			Vector3 velocity = controller.velocity;
			if (!isFlying)
				velocity = velocity.SetY(0);
			movementPlayableDirector.time += velocity.magnitude * moveAnimSpeed * Time.deltaTime * Vector3.Dot(velocity, trs.forward);
			while (movementPlayableDirector.time < 0)
				movementPlayableDirector.time += movementPlayableDirector.duration;
			movementPlayableDirector.Evaluate();
		}

		public void HandleDrag ()
		{
			float velocityMultiplier = 1f - Time.deltaTime * drag;
			if (velocityMultiplier < 0)
				velocityMultiplier = 0;
			float slideVelocityMagnitudeSqr = slideVelocity.sqrMagnitude;
			if (slideVelocityMagnitudeSqr > 0)
			{
				float slideVelocityMagnitude = Mathf.Sqrt(slideVelocityMagnitudeSqr);
				slideVelocity = Vector3.ClampMagnitude(slideVelocity, slideVelocityMagnitude * velocityMultiplier);
			}
			float pushVelocityMagnitudeSqr = pushVelocity.sqrMagnitude;
			if (pushVelocityMagnitudeSqr > 0)
			{
				float pushVelocityMagnitude = Mathf.Sqrt(pushVelocityMagnitudeSqr);
				pushVelocity = Vector3.ClampMagnitude(pushVelocity, pushVelocityMagnitude * velocityMultiplier);
			}
		}
		
		public void HandleGravity ()
		{
			if (!isFlying && !isGrounded)
				move += Physics.gravity * Time.deltaTime;
		}

		public virtual void TakeDamage (float amount)
		{
			hp = Mathf.Clamp(hp - amount, 0, maxHp);
			if (hp == 0 && !dead)
				Death ();
		}

		public virtual void Death ()
		{
			dead = true;
		}

		void OnControllerColliderHit (ControllerColliderHit hit)
		{
			HandleSlopes ();
			Entity entity = hit.gameObject.GetComponentInParent<Entity>();
			if (entity != null)
				HandlePushing (entity);
		}

		void HandleSlopes ()
		{
			RaycastHit hit;
			if (Physics.Raycast(groundCheckPoint.position, Vector3.down, out hit, groundCheckDistance, whatICollideWith))
			{
				float slopeAngle = Vector3.Angle(hit.normal, Vector3.up);
				if (slopeAngle > controller.slopeLimit)
				{
					Vector3 normal = hit.normal;
					Vector3 tangent = Vector3.down;
					Vector3.OrthoNormalize(ref normal, ref tangent);
					slideVelocity += tangent * Vector3.Dot(tangent, Vector3.down) * slideRate * Time.deltaTime;
				}
			}
		}

		void HandlePushing (Entity entity)
		{
			Vector3 myVelocity = controller.velocity;
			Vector3 entityVelocity = entity.controller.velocity;
			pushVelocity += entityVelocity * (entity.mass / mass);
			entity.pushVelocity += myVelocity * (mass / entity.mass);
		}
	}
}