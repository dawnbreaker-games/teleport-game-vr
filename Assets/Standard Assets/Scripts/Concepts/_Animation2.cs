#if UNITY_EDITOR
using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Formats.Alembic.Importer;

namespace VRGame
{
	[Serializable]
	public class _Animation2
	{
		public float Duration
		{
			get
			{
				return endTime - startTime;
			}
		}
		public string name;
		public float playRate;
		public float currentTime;
		public float startTime;
		public float endTime;
		public AlembicStreamPlayer alembicStreamPlayer;
		public ColliderEntry[] colliderEntries = new ColliderEntry[0];
		public WrapMode wrapMode;
		public bool autoPlay;
		[HideInInspector]
		public _Animator2 animator;
		Coroutine playRoutine;
		int playDirection;

		public void SetCurrentTime (float time)
		{
			currentTime = time;
			time = Mathf.Clamp(time, startTime, endTime);
			alembicStreamPlayer.UpdateImmediately(time);
			if (playDirection == 1)
			{
				for (int i = 0; i < colliderEntries.Length; i ++)
				{
					ColliderEntry colliderEntry = colliderEntries[i];
					if (time >= colliderEntry.currentColliderIndex * colliderEntry.changeInterval)
					{
						colliderEntry.currentCollider.enabled = false;
						colliderEntry.currentCollider = colliderEntry.meshColliders[colliderEntry.currentColliderIndex];
						colliderEntry.currentCollider.enabled = true;
						colliderEntry.currentColliderIndex ++;
						colliderEntries[i] = colliderEntry;
					}
				}
			}
			else
			{
				for (int i = 0; i < colliderEntries.Length; i ++)
				{
					ColliderEntry colliderEntry = colliderEntries[i];
					if (time <= colliderEntry.currentColliderIndex * colliderEntry.changeInterval)
					{
						colliderEntry.currentCollider.enabled = false;
						colliderEntry.currentCollider = colliderEntry.meshColliders[colliderEntry.currentColliderIndex];
						colliderEntry.currentCollider.enabled = true;
						colliderEntry.currentColliderIndex --;
						colliderEntries[i] = colliderEntry;
					}
				}
			}
		}
		
		public void Play (bool playForwards = true)
		{
			if (playForwards)
				playDirection = 1;
			else
				playDirection = -1;
			playRoutine = animator.StartCoroutine(PlayRoutine ());
		}
		
		public void Stop ()
		{
			if (playRoutine != null)
				animator.StopCoroutine(playRoutine);
		}
		
		public IEnumerator PlayRoutine ()
		{
			currentTime = startTime;
			for (int i = 0; i < colliderEntries.Length; i ++)
			{
				ColliderEntry colliderEntry = colliderEntries[i];
				colliderEntry.currentColliderIndex = 0;
				colliderEntry.currentCollider.enabled = false;
				colliderEntry.currentCollider = colliderEntry.meshColliders[0];
				colliderEntry.currentCollider.enabled = true;
				colliderEntries[i] = colliderEntry;
			}
			while (true)
			{
				currentTime += playRate * playDirection * Time.deltaTime;
				SetCurrentTime (currentTime);
				if (wrapMode == WrapMode.Once && currentTime >= endTime)
					yield break;
				else if (wrapMode == WrapMode.Loop)
				{
					while (currentTime >= endTime)
						currentTime -= Duration;
				}
				else if (wrapMode == WrapMode.PingPong)
				{
					if (currentTime >= endTime)
					{
						currentTime = endTime + (endTime - currentTime);
						playDirection *= -1;
					}
					else if (currentTime <= startTime)
					{
						currentTime = startTime + (startTime - currentTime);
						playDirection *= -1;
					}
				}
				yield return new WaitForEndOfFrame();
			}
		}

		[Serializable]
		public struct ColliderEntry
		{
			public MeshCollider[] meshColliders;
			public MeshCollider currentCollider;
			public int currentColliderIndex;
			public float changeInterval;

			public ColliderEntry (GameObject go, float animationEndTime)
			{
				meshColliders = go.GetComponents<MeshCollider>();
				currentCollider = meshColliders[0];
				currentColliderIndex = 0;
				changeInterval = animationEndTime / meshColliders.Length;
			}
		}
		
		public enum WrapMode
		{
			Once,
			Loop,
			PingPong
		}
	}
}
#endif