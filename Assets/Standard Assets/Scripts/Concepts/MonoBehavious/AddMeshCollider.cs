using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace VRGame
{
	public class AddMeshCollider : MonoBehaviour
	{
        void Start ()
        {
            gameObject.AddComponent<MeshCollider>();
            Destroy(this);
        }
	}
}