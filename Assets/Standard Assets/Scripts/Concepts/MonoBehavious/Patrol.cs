﻿using Extensions;
using Pathfinding;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace VRGame
{
	public class Patrol : Entity
	{
		public Enemy enemy;
		public float patrolRange;
		public float stopRange;
		public bool patrolLand;
		public bool patrolWater;
		public LayerMask whatIsLand;
		public LayerMask whatIsWater;
		public Seeker seeker;
		public float raiseAmountOnStuck;
		public bool shouldSetDestination;
		public Path path;
		[HideInInspector]
		public Vector3 centerOfPatrolArea;
		[HideInInspector]
		public Vector3 destination;
		int framesSinceStartedPath;
		float stopRangeSqr;
		Vector3 toDestination;
		Vector3 toPathPoint;
		Vector3 previousPosition;

		void Awake ()
		{
			centerOfPatrolArea = trs.position;
			stopRangeSqr = stopRange * stopRange;
		}

		public override void OnEnable ()
		{
			SetDestination ();
			base.OnEnable ();
		}

		public override void DoUpdate ()
		{
			if (isFlying)
			{
				toDestination = destination - trs.position;
				move = Vector3.ClampMagnitude(toDestination, 1) * moveSpeed;
				Rotate (move);
				controller.Move(move * Time.deltaTime);
				if (shouldSetDestination && toDestination.sqrMagnitude <= stopRangeSqr)
					SetDestination ();
			}
			else if (path.vectorPath.Count > 0)
			{
				toPathPoint = (path.vectorPath[0] - trs.position).SetY(0);
				move = (Vector3.ClampMagnitude(toPathPoint, 1) * moveSpeed).SetY(move.y);
				Rotate (toPathPoint);
				HandleGravity ();
				controller.Move(move * Time.deltaTime);
				if (toPathPoint.sqrMagnitude <= stopRangeSqr)
					path.vectorPath.RemoveAt(0);
				if (framesSinceStartedPath > 0)
				{
					if ((trs.position - previousPosition).SetY(0).sqrMagnitude <= (moveSpeed / 2 * Time.deltaTime) * (moveSpeed / 2 * Time.deltaTime))
						trs.position += Vector3.up * raiseAmountOnStuck;
				}
				framesSinceStartedPath ++;
			}
			else if (shouldSetDestination)
				SetDestination ();
			Vector3 velocity = controller.velocity;
			if (!isFlying)
				velocity = velocity.SetY(0);
			movementPlayableDirector.time += velocity.magnitude * moveAnimSpeed * Time.deltaTime * Vector3.Dot(velocity, trs.forward);
			while (movementPlayableDirector.time < 0)
				movementPlayableDirector.time += movementPlayableDirector.duration;
			movementPlayableDirector.Evaluate();
			previousPosition = trs.position;
		}

		void Rotate (Vector3 forward)
		{
			float angleToNewRotation = Vector3.Angle(trs.forward, forward);
			while (angleToNewRotation > enemy.visionDegrees)
			{
				trs.forward = Vector3.RotateTowards(trs.forward, forward, enemy.visionDegrees, 0);
				enemy.OnTriggerStay (Player.instance.controller);
				angleToNewRotation -= enemy.visionDegrees;
			}
			trs.forward = forward;
		}

		public void SetDestination ()
		{
			if (isFlying)
				SetDestination (centerOfPatrolArea + Random.onUnitSphere * Random.value * patrolRange);
			else
			{
				while (true)
				{
					destination = centerOfPatrolArea + (Random.insideUnitCircle * patrolRange).XYToXZ();
					RaycastHit hit;
					if (Physics.Raycast(destination.SetY(trs.position.y + patrolRange), Vector3.down, out hit, Mathf.Infinity, whatIsLand.AddToMask(whatIsWater)))
					{
						bool shouldReturn = true;
						if (!patrolLand && LayerMaskExtensions.MaskContainsLayer(whatIsLand, hit.collider.gameObject.layer))
							shouldReturn = false;
						else if (!patrolWater && LayerMaskExtensions.MaskContainsLayer(whatIsWater, hit.collider.gameObject.layer))
							shouldReturn = false;
						if (shouldReturn)
						{
							SetDestination (destination);
							return;
						}
					}
				}
			}
		}

		public void SetDestination (Vector3 position)
		{
			destination = position;
			if (!isFlying)
			{
				path = seeker.StartPath(trs.position, destination);
				framesSinceStartedPath = 0;
			}
		}

		void OnCollisionStay (Collision coll)
		{
			if (shouldSetDestination && coll.gameObject.GetComponent<Enemy>() != null)
				SetDestination ();
		}
	}
}