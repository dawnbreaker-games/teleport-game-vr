#if UNITY_EDITOR
using UnityEngine;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;

namespace VRGame
{
	public class _Animator2 : MonoBehaviour
	{
		public _Animation2[] animations = new _Animation2[0];
		List<_Animation2> currentlyPlayingAnimations = new List<_Animation2>();
		Dictionary<string, _Animation2> animationDict = new Dictionary<string, _Animation2>();
		
		void Awake ()
		{
			for (int i = 0; i < animations.Length; i ++)
			{
				_Animation2 animation = animations[i];
				animation.animator = this;
				animationDict.Add(animation.name, animation);
			}
		}

		void OnEnable ()
		{
			for (int i = 0; i < animations.Length; i ++)
			{
				_Animation2 animation = animations[i];
				if (animation.autoPlay)
					animation.Play ();
			}
		}
		
		public void Play (_Animation2 animation, bool playForwards = true)
		{
			animation.Play (playForwards);
			currentlyPlayingAnimations.Add(animation);
		}
		
		public void Play (string animationName, bool playForwards = true)
		{
			Play (animationDict[animationName], playForwards);
		}
		
		public void Play (int animationIndex, bool playForwards = true)
		{
			Play (animations[animationIndex], playForwards);
		}

		public void StopAll ()
		{
			while (currentlyPlayingAnimations.Count > 0)
				Stop (currentlyPlayingAnimations[0]);
		}
		
		public void Stop ()
		{
			if (currentlyPlayingAnimations.Count > 0)
				Stop (currentlyPlayingAnimations[0]);
		}
		
		public void Stop (_Animation2 animation)
		{
			animation.Stop ();
			currentlyPlayingAnimations.Remove(animation);
		}
		
		public void Stop (string animationName)
		{
			Stop (animationDict[animationName]);
		}
		
		public void Stop (int animationIndex)
		{
			Stop (animations[animationIndex]);
		}
		
		public List<string> GetCurrentlyPlayingAnimationNames ()
		{
			List<string> output = new List<string>();
			for (int i = 0; i < currentlyPlayingAnimations.Count; i ++)
			{
				_Animation2 animation = animations[i];
				output.Add(animation.name);
			}
			return output;
		}

		void OnDisable ()
		{
			StopAllCoroutines();
		}
	}
}
#endif