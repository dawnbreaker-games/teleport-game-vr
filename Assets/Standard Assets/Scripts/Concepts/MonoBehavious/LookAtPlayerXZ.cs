﻿using VRGame;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LookAtPlayerXZ : UpdateWhileEnabled
{
	public Transform trs;
	
	public override void DoUpdate ()
	{
		trs.forward = (Player.instance.trs.position - trs.position).GetXZ();
	}
}
