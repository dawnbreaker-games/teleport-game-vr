﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace VRGame
{
	public class Hazard : Spawnable
	{
		public float damage;
		public bool destroyOnContact;
		[HideInInspector]
		public bool dead;
		
		public virtual void OnCollisionEnter (Collision coll)
		{
			if (dead)
				return;
			IDestructable destructable = coll.gameObject.GetComponent<IDestructable>();
			if (destructable != null)
				ApplyDamage (destructable, damage);
			if (destroyOnContact)
			{
				dead = true;
				if (prefabIndex == -1)
					Destroy(gameObject);
				else
					// ObjectPool.Instance.Despawn (prefabIndex, gameObject, trs);
					Destroy(gameObject);
			}
		}

		void OnTriggerEnter (Collider other)
		{
			if (dead)
				return;
			IDestructable destructable = other.GetComponentInParent<IDestructable>();
			if (destructable != null)
				ApplyDamage (destructable, damage);
			if (destroyOnContact)
			{
				dead = true;
				if (prefabIndex == -1)
					Destroy(gameObject);
				else
					// ObjectPool.Instance.Despawn (prefabIndex, gameObject, trs);
					Destroy(gameObject);
			}
		}
		
		public virtual void ApplyDamage (IDestructable destructable, float amount)
		{
			destructable.TakeDamage (amount);
		}
	}
}