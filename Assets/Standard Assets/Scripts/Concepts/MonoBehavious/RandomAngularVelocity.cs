using UnityEngine;

public class RandomAngularVelocity : MonoBehaviour
{
	public Rigidbody rigid;
	public FloatRange magnitude;

	void OnEnable ()
	{
		rigid.angularVelocity = Random.onUnitSphere * magnitude.Get(Random.value);
	}
}