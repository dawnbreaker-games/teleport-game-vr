using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRGame;

namespace VRGame
{
	public class MagicLocater : SingletonMonoBehaviour<MagicLocater>
	{
		public bool PauseWhileUnfocused
		{
			get
			{
				return true;
			}
		}
		public Transform trs;
		public float range;
	}
}