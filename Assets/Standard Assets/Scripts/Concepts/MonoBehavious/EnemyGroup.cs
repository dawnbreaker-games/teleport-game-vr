﻿using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace VRGame
{
	public class EnemyGroup : UpdateWhileEnabled
	{
		public bool PauseWhileUnfocused
		{
			get
			{
				return true;
			}
		}
#if UNITY_EDITOR
		public bool useCustomYPosition;
		public float yPosition;
#endif
		public Transform trs;
		public Enemy[] enemies;
		Patrol patrol;

		void Awake ()
		{
			float patrolArea = 0;
			for (int i = 0; i < enemies.Length; i ++)
			{
				Enemy enemy = enemies[i];
				patrol = enemy.patrol;
				patrolArea += new Circle2D(patrol.patrolRange).Area - new Circle2D(patrol.trs.lossyScale.x * patrol.controller.radius).Area;
				patrol.centerOfPatrolArea = trs.position;
				patrol.shouldSetDestination = false;
			}
			patrol.patrolRange = Mathf.Sqrt(patrolArea / Mathf.PI);
			SetDestination ();
		}

		public override void DoUpdate ()
		{
			for (int i = 0; i < enemies.Length; i ++)
			{
				Enemy enemy = enemies[i];
				if (enemy.patrol.path.IsDone() && enemy.patrol.path.vectorPath.Count == 0)
					SetDestination ();
			}
		}

		void SetDestination ()
		{
			patrol.SetDestination ();
			for (int i = 0; i < enemies.Length - 1; i ++)
			{
				Enemy enemy = enemies[i];
				enemy.patrol.SetDestination (patrol.destination);
			}
		}
	}
}