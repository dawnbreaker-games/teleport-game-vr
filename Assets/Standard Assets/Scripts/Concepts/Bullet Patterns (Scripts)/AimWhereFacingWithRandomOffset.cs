﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace VRGame
{
	[CreateAssetMenu]
	public class AimWhereFacingWithRandomOffset : AimWhereFacing
	{
		public float randomShootOffsetRange;
		
		public override Bullet[] Shoot (Transform spawner, Bullet bulletPrefab, float positionOffset = 0)
		{
			spawner.forward = GetShootDirection(spawner);
			return base.Shoot(spawner, bulletPrefab, positionOffset);
		}
		
		public override Vector3 GetShootDirection (Transform spawner)
		{
			return Quaternion.Euler(Random.onUnitSphere * Random.Range(0, randomShootOffsetRange)) * base.GetShootDirection(spawner);
		}
	}
}