﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace VRGame
{
	[CreateAssetMenu]
	public class AimAtPlayerWithRandomOffset : AimAtPlayer
	{
		public float randomShootOffsetRange;
		
		public override Vector3 GetShootDirection (Transform spawner)
		{
			return Quaternion.Euler(Random.onUnitSphere * Random.Range(0, randomShootOffsetRange)) * base.GetShootDirection(spawner);
		}
	}
}