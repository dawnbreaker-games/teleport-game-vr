﻿using UnityEngine;
using Extensions;
using System.Collections;
using System.Collections.Generic;

namespace VRGame
{
	[CreateAssetMenu]
	public class AimWhereFacingThenRotate : AimWhereFacing
	{
		public Quaternion rotate;
		
		public override Bullet[] Shoot (Transform spawner, Bullet bulletPrefab, float positionOffset = 0)
		{
			Bullet[] output = base.Shoot(spawner, bulletPrefab, positionOffset);
			spawner.forward = rotate * spawner.forward;
			return output;
		}
	}
}