using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace VRGame
{
	public class Weapon : MonoBehaviour
	{
		public Transform trs;
		public Animation anim;
		public AnimationClip attackAnim;
		public float damage;
		
		public virtual void Attack ()
		{
            if (attackAnim != null)
			    anim.Play(attackAnim.name);
		}
	}
}