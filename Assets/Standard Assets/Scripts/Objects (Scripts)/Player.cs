﻿using TMPro;
using System;
using Extensions;
using UnityEngine;
using UnityEngine.Playables;
using System.Collections.Generic;

namespace VRGame
{
	public class Player : Entity
	{
		public static Player instance;
		public static Player Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<Player>();
				return instance;
			}
			set
			{
				instance = value;
			}
		}
		public TMP_Text hpText;
		public Weapon leftHandWeapon;
		public Weapon rightHandWeapon;
		public Transform leftHandTeleportIndicatorTrs;
		public Transform rightHandTeleportIndicatorTrs;
		public float teleportRange;
		public float sampleInterval;
		bool leftGripInput;
		bool previousLeftGripInput;
		bool rightGripInput;
		bool previousRightGripInput;
		
		void Start ()
		{
			Hp = MaxHp;
			leftHandTeleportIndicatorTrs.SetParent(null);
			rightHandTeleportIndicatorTrs.SetParent(null);
		}

		public override void DoUpdate ()
		{
			if (dead)
				return;
			leftGripInput = InputManager.LeftGripInput;
			rightGripInput = InputManager.RightGripInput;
			HandleAttacking ();
			HandleTeleport ();
			previousLeftGripInput = leftGripInput;
			previousRightGripInput = rightGripInput;
		}

		void HandleTeleport ()
		{
			Vector3 closestLeftHandPoint = new Vector3();
			Vector3 closestRightHandPoint = new Vector3();
			LineSegment3D leftHandLineSegment = new LineSegment3D(OVRCameraRig.instance.leftHandTrs.position, OVRCameraRig.instance.leftHandTrs.position + OVRCameraRig.instance.leftHandTrs.forward * teleportRange);
			LineSegment3D rightHandLineSegment =  new LineSegment3D(OVRCameraRig.instance.rightHandTrs.position, OVRCameraRig.instance.rightHandTrs.position + OVRCameraRig.instance.rightHandTrs.forward * teleportRange);
			// ClosestPointsOn2LineSegments (leftHandLineSegment, rightHandLineSegment, out closestLeftHandPoint, out closestRightHandPoint);
			float closestDistance = Mathf.Infinity;
			for (float distanceTraveled = 0; distanceTraveled < teleportRange + sampleInterval; distanceTraveled += sampleInterval)
			{
				distanceTraveled = Mathf.Clamp(distanceTraveled, 0, teleportRange);
				Vector3 leftHandPoint = leftHandLineSegment.GetPointWithDirectedDistance(distanceTraveled);
				Vector3 rightHandPoint = rightHandLineSegment.GetPointWithDirectedDistance(distanceTraveled);
				float distance = (leftHandPoint - rightHandPoint).sqrMagnitude;
				if (distance < closestDistance)
				{
					closestDistance = distance;
					closestLeftHandPoint = leftHandPoint;
					closestRightHandPoint = rightHandPoint;
				}
				else
					break;
			}
			leftHandTeleportIndicatorTrs.position = closestLeftHandPoint;
			rightHandTeleportIndicatorTrs.position = closestRightHandPoint;
			if (leftGripInput && !previousLeftGripInput)
				Teleport (closestLeftHandPoint);
			else if (rightGripInput && !previousRightGripInput)
				Teleport (closestRightHandPoint);
		}

		void Teleport (Vector3 point)
		{
			RaycastHit hit;
			if (Physics.Raycast(trs.position, point - trs.position, out hit, teleportRange, whatICollideWith))
				trs.position = hit.point;
			else
				trs.position = point;
		}

		Vector3 ConstrainToLineSegment (Vector3 position, LineSegment3D lineSegment3D)
		{
			Vector3 ba = lineSegment3D.end - lineSegment3D.start;
			float t = Vector3.Dot(position - lineSegment3D.start, ba) / Vector3.Dot(ba, ba);
			return Vector3.Lerp(lineSegment3D.start, lineSegment3D.end, Mathf.Clamp01(t));
		}

		void ClosestPointsOn2LineSegments (LineSegment3D lineSegment3D, LineSegment3D lineSegment3D2, out Vector3 closestPointOnSegment, out Vector3 closestPointOnSegment2)
		{
			Vector3 segDC = lineSegment3D2.end - lineSegment3D2.start;
			float lineDirSqrMag = Vector3.Dot(segDC, segDC);
			Vector3 inPlaneA = lineSegment3D.start - ((Vector3.Dot(lineSegment3D.start - lineSegment3D2.start, segDC) / lineDirSqrMag) *segDC);
			Vector3 inPlaneB = lineSegment3D.end - ((Vector3.Dot(lineSegment3D.end - lineSegment3D2.start, segDC) / lineDirSqrMag) * segDC);
			Vector3 inPlaneBA = inPlaneB - inPlaneA;
			float t = Vector3.Dot(lineSegment3D2.start - inPlaneA, inPlaneBA) / Vector3.Dot(inPlaneBA, inPlaneBA);
			t = (inPlaneA != inPlaneB) ? t : 0f;
			Vector3 segABtoLineCD = Vector3.Lerp(lineSegment3D.start, lineSegment3D.end, Mathf.Clamp01(t));
			closestPointOnSegment2 = ConstrainToLineSegment(segABtoLineCD, lineSegment3D2);
			closestPointOnSegment = ConstrainToLineSegment(closestPointOnSegment2, lineSegment3D);
		}

		void HandleAttacking ()
		{
			if (InputManager.LeftTriggerInput)
				leftHandWeapon.Attack ();
			if (InputManager.RightTriggerInput)
				rightHandWeapon.Attack ();
		}
		
		public override void Death ()
		{
			base.Death ();
			LoseableScenerio.Instance.Lose ();
		}

		public override void TakeDamage (float amount)
		{
			hp = Mathf.Clamp(hp - amount, 0, maxHp);
			hpText.text = "Health: " + hp;
			if (hp == 0 && !dead)
				Death ();
		}
	}
}