﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace VRGame
{
	public class Bomb : Bullet
	{
		bool exploded;
		public bool explodeOnContact;
		public Explosion explosionPrefab;
		public Timer explodeDelayTimer;

		void Awake ()
		{
			explodeDelayTimer.onFinished += Explode;
		}

		public override void OnDisable ()
		{
			base.OnDisable ();
			exploded = false;
		}

		public override void OnDestroy ()
		{
			base.OnDestroy ();
			explodeDelayTimer.onFinished -= Explode;
		}

		public override void OnCollisionEnter (Collision coll)
		{
			if (!exploded && explodeOnContact)
			{
				exploded = true;
				explodeDelayTimer.Reset ();
				explodeDelayTimer.Start ();
			}
			base.OnCollisionEnter (coll);
		}

		public void Explode (params object[] args)
		{
			exploded = true;
			ObjectPool.Instance.SpawnComponent<Explosion> (explosionPrefab, trs.position);
			// ObjectPool.Instance.Despawn (prefabIndex, gameObject, trs);
			Destroy(gameObject);
		}
	}
}