﻿using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace VRGame
{
	public class ShootingEnemy : Enemy
	{
		public AttackEntry[] attackEntries;

		public override void Awake ()
		{
			base.Awake ();
			for (int i = 0; i < attackEntries.Length; i ++)
			{
				AttackEntry attackEntry = attackEntries[i];
				if (attackEntry.attackOnAnimationFrameIndex == -1)
				{
					attackEntries = attackEntries.RemoveAt(i);
					i --;
				}
			}
		}
		
		public override void OnEnable ()
		{
			base.OnEnable ();
			anim.animationDict[attackAnimName].onFrameChanged += OnAttackAnimationChanged;
		}
		
		public override void OnDisable ()
		{
			base.OnDisable ();
			anim.animationDict[attackAnimName].onFrameChanged -= OnAttackAnimationChanged;
		}
		
		public override void DoUpdate ()
		{
			if (!interestedInPlayer || dead)
				return;
			base.DoUpdate ();
			HandleAttacking ();
		}
		
		public virtual void HandleAttacking ()
		{
			if (anim != null && !anim.GetCurrentlyPlayingAnimationNames().Contains(attackAnimName))
			{
				anim.StopAll (true);
				anim.Play (attackAnimName);
			}
#if UNITY_EDITOR
			if (anim2 != null && !anim2.GetCurrentlyPlayingAnimationNames().Contains(attackAnimName))
			{
				anim2.StopAll ();
				anim2.Play (attackAnimName);
			}
#endif
		}
		
		public virtual void OnAttackAnimationChanged (int animationFrameIndex)
		{
			for (int i = 0; i < attackEntries.Length; i ++)
			{
				AttackEntry attackEntry = attackEntries[i];
				if (attackEntry.attackOnAnimationFrameIndex == animationFrameIndex)
					attackEntry.Attack ();
			}
		}
	}
}