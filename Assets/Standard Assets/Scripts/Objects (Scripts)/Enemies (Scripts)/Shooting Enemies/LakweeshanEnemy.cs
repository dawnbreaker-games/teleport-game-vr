﻿using Extensions;
using UnityEngine;
using System.Collections;
using UnityEngine.Playables;
using System.Collections.Generic;

namespace VRGame
{
	public class LakweeshanEnemy : Enemy
	{
		public float dartSpeed;
		public float dartStopDelay;
		public float maxDartDistance;
		public float damage;
		public ColliderPresenceDetector playerDetector;
		public CapsuleCollider capsuleCollider;
		public float attackRange;
		public PlayableDirector attackPlayableDirector; 
		public float attackAnimSpeed;
		float maxDartDistanceSqr;
		bool isDarting;
		float distToCapsuleSphere;
		float attackRangeSqr;

		public override void Awake ()
		{
			base.Awake ();
			maxDartDistanceSqr = maxDartDistance * maxDartDistance;
			attackRangeSqr = attackRange * attackRange;
		}

		public override void DoUpdate ()
		{
			if (!interestedInPlayer || dead)
				return;
			toPlayer = Player.instance.trs.position - trs.position;
			HandleDarting ();
			HandleAttacking ();
			base.DoUpdate ();
		}

		void HandleDarting ()
		{
			if (!isDarting && toPlayer.SetY(0).sqrMagnitude <= maxDartDistanceSqr)
			{
				if (Physics.CapsuleCast(trs.position - trs.up * capsuleCollider.height / 2, trs.position + trs.up * capsuleCollider.height / 2, capsuleCollider.radius, toPlayer.SetY(0), maxDartDistance, LayerMask.GetMask(LayerMask.LayerToName(Player.instance.gameObject.layer))))
				{
					trs.forward = toPlayer.SetY(0);
					isDarting = true;
					attackPlayableDirector.time = 0;
					attackPlayableDirector.Evaluate();
					StartCoroutine(DartRoutine ());
				}
			}
		}

		void HandleAttacking ()
		{
			if (!anim.GetCurrentlyPlayingAnimationNames().Contains(attackAnimName))
			{
				anim.Stop (true);
				if (toPlayer.sqrMagnitude <= attackRangeSqr)
				{
					anim.Play (attackAnimName);
					playerDetector.enabled = true;
				}
			}
			else if (playerDetector.collidersInside.Count > 0)
			{
				Player.instance.TakeDamage (damage);
				playerDetector.enabled = false;
			}
		}

		public override void HandleMovement ()
		{
			if (toPlayer.sqrMagnitude >= followDistanceRangeSqr.max)
				Move ();
			else if (toPlayer.sqrMagnitude <= followDistanceRangeSqr.min)
			{
				if (isDarting)
					move = (-toPlayer * dartSpeed).SetY(move.y);
				else
				{
					move = (-toPlayer * moveSpeed).SetY(move.y);
					trs.forward = toPlayer.SetY(0);
				}
			}
			else
			{
				move = Vector3.up * move.y;
				trs.forward = toPlayer.SetY(0);
			}
		}

		IEnumerator DartRoutine ()
		{
			bool previousShouldStop = false;
			float timeSinceShouldStop = Mathf.Infinity;
			while (true)
			{
				yield return new WaitForEndOfFrame();
				attackPlayableDirector.time += attackAnimSpeed * Time.deltaTime;
				attackPlayableDirector.Evaluate();
				if (!Physics.CapsuleCast(trs.position - trs.up * capsuleCollider.height / 2, trs.position + trs.up * capsuleCollider.height / 2, capsuleCollider.radius, trs.forward, maxDartDistance, LayerMask.GetMask(LayerMask.LayerToName(Player.instance.gameObject.layer))))
				{
					if (!previousShouldStop)
						timeSinceShouldStop = Time.time;
					else if (Time.time - timeSinceShouldStop > dartStopDelay)
						break;
					previousShouldStop = true;
				}
				else
					previousShouldStop = false;
			}
			isDarting = false;
		}

		public override void Move ()
		{
			if (isDarting)
				move = (trs.forward * dartSpeed).SetY(move.y);
			else
				base.Move ();
		}
	}
}