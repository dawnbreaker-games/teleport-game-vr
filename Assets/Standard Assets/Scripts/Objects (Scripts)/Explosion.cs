using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace VRGame
{
	public class Explosion : Hazard
	{
		void OnTriggerEnter (Collider other)
		{
			if (dead)
				return;
			dead = true;
			IDestructable destructable = other.GetComponent<IDestructable>();
			if (destructable != null)
				ApplyDamage (destructable, damage);
		}

		public void DestroyMe ()
		{
			Destroy(gameObject);
		}
	}
}