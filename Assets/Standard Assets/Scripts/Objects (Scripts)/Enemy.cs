using Extensions;
using Pathfinding;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace VRGame
{
	public class Enemy : Entity, ISpawnable
	{
		public int prefabIndex;
		public int PrefabIndex
		{
			get
			{
				return prefabIndex;
			}
		}
		public FloatRange followDistanceRange;
		[HideInInspector]
		public bool interestedInPlayer;
		public float interestRange;
		[HideInInspector]
		public float interestRangeSqr;
		public _Animator anim;
#if UNITY_EDITOR
		public _Animator2 anim2;
#endif
		public Patrol patrol;
		public GameObject[] deadBodyGos = new GameObject[0];
		public string idleAnimName;
		public string hurtAnimName;
		public string attackAnimName;
		public string dieAnimName;
		public int despawnDelay;
		[HideInInspector]
		public Vector3 toPlayer;
		public LayerMask whatBlocksVision;
		public SphereCollider visionRangeSphereCollider;
		public Transform eyeTrs;
		public float visionDegrees;
		public EnemyGroup enemyGroup;
		public MakeMagicIndicator makeMagicIndicator;
		public Seeker seeker;
		public float stopRange;
		public float repathInterval;
		[HideInInspector]
		public FloatRange followDistanceRangeSqr;
		float repathTimer;
		Path path;
		float stopRangeSqr;
		
		public virtual void Awake ()
		{
			anim.Init ();
			interestRangeSqr = interestRange * interestRange;
			followDistanceRangeSqr = new FloatRange(followDistanceRange.min * followDistanceRange.min, followDistanceRange.max * followDistanceRange.max);
			stopRangeSqr = stopRange * stopRange;
#if UNITY_EDTIOR
			if (anim2 != null)
				anim2.Play (attackAnimName);
#endif
		}
		
		public override void OnEnable ()
		{
			for (int i = 0; i < deadBodyGos.Length; i ++)
			{
				GameObject deadBodyGo = deadBodyGos[i];
				deadBodyGo.layer = LayerMask.NameToLayer("Enemy");
			}
			dead = false;
			anim.Stop (true);
			anim.Play (idleAnimName);
			visionRangeSphereCollider.enabled = true;
			makeMagicIndicator.enabled = true;
			hp = maxHp;
		}
		
		public override void DoUpdate ()
		{
			if (!interestedInPlayer || dead)
				return;
			toPlayer = Player.instance.trs.position - trs.position;
			if (toPlayer.sqrMagnitude > interestRangeSqr)
				LoseInterest ();
			if (!isFlying)
			{
				repathTimer -= Time.deltaTime;
				if (repathTimer <= 0)
				{
					repathTimer += repathInterval;
					path = seeker.StartPath(trs.position, Player.instance.trs.position);
				}
			}
			HandleMovement ();
			base.DoUpdate ();
		}
		
		public virtual void HandleMovement ()
		{
			if (toPlayer.sqrMagnitude >= followDistanceRangeSqr.max)
				Move ();
			else if (toPlayer.sqrMagnitude <= followDistanceRangeSqr.min)
			{
				move = (-toPlayer.normalized * moveSpeed).SetY(move.y);
				trs.forward = toPlayer.SetY(0);
			}
			else
				move = Vector3.up * move.y;
		}
		
		public virtual void Move ()
		{
			if (!isFlying)
			{
				if (path != null && path.vectorPath.Count > 0)
				{
					Vector3 toPathPoint = (path.vectorPath[0] - trs.position).SetY(0);
					move = (toPathPoint.normalized * moveSpeed).SetY(move.y);
					if (toPathPoint.sqrMagnitude <= stopRangeSqr)
						path.vectorPath.RemoveAt(0);
					trs.forward = move.SetY(0);
				}
			}
			else
			{
				move = toPlayer.normalized * moveSpeed;
				trs.forward = move;
			}
		}
		
		public override void TakeDamage (float amount)
		{
			if (dead)
				return;
			float previousHp = hp;
			hp = Mathf.Clamp(hp - amount, 0, maxHp);
			if (Survival.Instance != null)
				Survival.instance.AddScore (previousHp - hp);
			// anim.Stop (true);
			if (!string.IsNullOrEmpty(hurtAnimName))
				anim.Play (hurtAnimName);
			if (hp == 0 && !dead)
				Death ();
			else if (!interestedInPlayer)
				Awaken ();
		}

		public virtual void OnDisable ()
		{
			StopAllCoroutines();
			GameManager.updatables = GameManager.updatables.Remove(this);
		}
		
		public virtual void OnTriggerStay (Collider other)
		{
			RaycastHit hit = new RaycastHit();
			if (!interestedInPlayer && Vector3.Angle(eyeTrs.forward, Player.instance.trs.position - eyeTrs.position) <= visionDegrees && Physics.Raycast(eyeTrs.position, Player.instance.trs.position - eyeTrs.position, out hit, visionRangeSphereCollider.bounds.extents.x, whatBlocksVision) && hit.collider == Player.instance.controller)
			{
				if (enemyGroup != null)
				{
					enemyGroup.enabled = false;
					for (int i = 0; i < enemyGroup.enemies.Length; i ++)
					{
						Enemy enemy = enemyGroup.enemies[i];
						if (!enemy.interestedInPlayer)
							enemy.Awaken ();
					}
				}
				else
					Awaken ();
			}
		}

		public virtual void Awaken ()
		{
			interestedInPlayer = true;
			if (patrol != null)
				patrol.enabled = false;
			visionRangeSphereCollider.enabled = false;
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		public virtual void LoseInterest ()
		{
			interestedInPlayer = false;
			if (enemyGroup != null)
			{
				for (int i = 0; i < enemyGroup.enemies.Length; i ++)
				{
					Enemy enemy = enemyGroup.enemies[i];
					if (enemy.interestedInPlayer)
						enemy.LoseInterest ();
				}
				enemyGroup.enabled = true;
			}
			else if (patrol != null)
				patrol.enabled = true;
			visionRangeSphereCollider.enabled = true;
			GameManager.updatables = GameManager.updatables.Remove(this);
		}
		
		public override void Death ()
		{
			dead = true;
			anim.Stop (true);
			anim.Play (dieAnimName);
			for (int i = 0; i < deadBodyGos.Length; i ++)
			{
				GameObject deadBodyGo = deadBodyGos[i];
				deadBodyGo.layer = LayerMask.NameToLayer("Dead Body");
			}
			SoundEffect.Settings deathSoundSettings = new SoundEffect.Settings();
			deathSoundSettings.audioClip = AudioManager.Instance.deathSounds[Random.Range(0, AudioManager.Instance.deathSounds.Length)];
			deathSoundSettings.speakerTrs = trs;
			AudioManager.Instance.MakeSoundEffect (deathSoundSettings);
			makeMagicIndicator.enabled = false;
			if (prefabIndex != -1)
				ObjectPool.Instance.DelayDespawn (prefabIndex, gameObject, trs, despawnDelay);
			else
				Destroy(gameObject, despawnDelay);
			GameManager.updatables = GameManager.updatables.Remove(this);
		}

		void OnControllerColliderHit (ControllerColliderHit hit)
		{
			HandleSlopes ();
		}

		void OnCollisionEnter (Collision coll)
		{
			if (!interestedInPlayer && coll.gameObject == Player.instance.gameObject)
				Awaken ();
		}

		void HandleSlopes ()
		{
			RaycastHit hit;
			if (Physics.Raycast(groundCheckPoint.position, Vector3.down, out hit, groundCheckDistance, whatICollideWith))
			{
				float slopeAngle = Vector3.Angle(hit.normal, Vector3.up);
				if (slopeAngle > controller.slopeLimit)
				{
					Vector3 normal = hit.normal;
					Vector3 tangent = Vector3.down;
					Vector3.OrthoNormalize(ref normal, ref tangent);
					slideVelocity += tangent * Vector3.Dot(tangent, Vector3.down) * slideRate * Time.deltaTime;
				}
			}
		}
	}
}