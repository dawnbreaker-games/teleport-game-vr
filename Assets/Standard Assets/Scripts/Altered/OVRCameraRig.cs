﻿using VRGame;
using System;
using Extensions;
using UnityEngine;
using System.Collections;
using UnityEngine.InputSystem;
using System.Collections.Generic;

public class OVRCameraRig : SingletonUpdateWhileEnabled<OVRCameraRig>
{
	public bool PauseWhileUnfocused
	{
		get
		{
			return true;
		}
	}
	public new Camera camera;
	public Transform trackingSpaceTrs;
	public Transform eyesTrs;
	public Transform leftHandTrs;
	public Transform rightHandTrs;
	public Transform trs;
	static Transform currentHand;
	public static Transform CurrentHand
	{
		get
		{
			return currentHand;
		}
		set
		{
			currentHand = value;
		}
	}
	public float lookRate;
	public LayerMask whatICollideWith;
	[HideInInspector]
	public float cameraDistance;
	Vector3 positionOffset;
	Quaternion rota;
	bool setOrientationInput;
	bool previousSetOrientationInput;
	
	public override void OnEnable ()
	{
		CurrentHand = rightHandTrs;
		positionOffset = trs.position - Player.Instance.trs.position;
		cameraDistance = positionOffset.magnitude;
		trs.SetParent(null);
		SetOrientation ();
		if (InputManager.Instance.inputDevice == InputManager.InputDevice.KeyboardAndMouse)
		{
			Cursor.visible = false;
			Cursor.lockState = CursorLockMode.Confined;
		}
		base.OnEnable ();
	}

	public override void DoUpdate ()
	{
		if (InputManager.Instance.inputDevice == InputManager.InputDevice.OculusRiftAndQuest)
			UpdateTransforms ();
		else
		{
			Vector2 rotaInput = Mouse.current.delta.ReadValue().FlipY() * lookRate * Time.deltaTime;
			if (rotaInput != Vector2.zero)
			{
				trs.RotateAround(Player.instance.trs.position, trs.right, rotaInput.y);
				trs.RotateAround(Player.instance.trs.position, Vector3.up, rotaInput.x);
				trs.LookAt(Player.instance.trs, Vector3.up);
			}
			RaycastHit hit;
			if (Physics.Raycast(Player.instance.trs.position, trs.position - Player.instance.trs.position, out hit, cameraDistance, whatICollideWith))
				trs.position = Player.instance.trs.position - trs.forward * hit.distance;
			else
				trs.position = Player.instance.trs.position - trs.forward * cameraDistance;
		}
		// setOrientationInput = InputManager.SetOrientationInput;
		// if (setOrientationInput && !previousSetOrientationInput)
		// 	SetOrientation ();
		// previousSetOrientationInput = setOrientationInput;
	}

	void UpdateTransforms ()
	{
		RaycastHit hit;
		if (Physics.Raycast(Player.instance.trs.position, trs.position - Player.instance.trs.position, out hit, cameraDistance, whatICollideWith))
			positionOffset = positionOffset.normalized * hit.distance;
		else
			positionOffset = positionOffset.normalized * cameraDistance;
		trs.position = Player.instance.trs.position + (rota * positionOffset);
		if (InputManager.HeadPosition != null)
			eyesTrs.localPosition = (Vector3) InputManager.HeadPosition;
		if (InputManager.HeadRotation != null)
			eyesTrs.localRotation = (Quaternion) InputManager.HeadRotation;
		if (InputManager.LeftHandPosition != null)
			leftHandTrs.localPosition = (Vector3) InputManager.LeftHandPosition;
		if (InputManager.RightHandPosition != null)
			rightHandTrs.localPosition = (Vector3) InputManager.RightHandPosition;
		if (InputManager.LeftHandRotation != null)
			leftHandTrs.localRotation = (Quaternion) InputManager.LeftHandRotation;
		if (InputManager.RightHandRotation != null)
			rightHandTrs.localRotation = (Quaternion) InputManager.RightHandRotation;
	}
	
	void SetOrientation ()
	{
		rota = eyesTrs.rotation;
		trackingSpaceTrs.forward = eyesTrs.forward.GetXZ();
	}
}