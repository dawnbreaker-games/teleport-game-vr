﻿using VRGame;
using System;
using UnityEngine;
using System.Collections;
using UnityEngine.InputSystem;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class GameManager : SingletonMonoBehaviour<GameManager>
{
	public static bool paused;
	public GameObject[] registeredGos = new GameObject[0];
	[SaveAndLoadValue]
	static string enabledGosString = "";
	[SaveAndLoadValue]
	static string disabledGosString = "";
	public static IUpdatable[] updatables = new IUpdatable[0];
	public static int framesSinceLevelLoaded;

	public override void Awake ()
	{
		base.Awake ();
		SceneManager.sceneLoaded += OnSceneLoaded;
	}

	public virtual void OnDestroy ()
	{
		SceneManager.sceneLoaded -= OnSceneLoaded;
	}
	
	public virtual void OnSceneLoaded (Scene scene = new Scene(), LoadSceneMode loadMode = LoadSceneMode.Single)
	{
		StopAllCoroutines();
		framesSinceLevelLoaded = 0;
	}

	public virtual void Update ()
	{
		// Physics.Simulate(Time.deltaTime);
		for (int i = 0; i < updatables.Length; i ++)
		{
			IUpdatable updatable = updatables[i];
			updatable.DoUpdate ();
		}
		if (ObjectPool.Instance != null && ObjectPool.instance.enabled)
			ObjectPool.instance.DoUpdate ();
		InputSystem.Update ();
		framesSinceLevelLoaded ++;
	}
	
	public virtual void Quit ()
	{
		Application.Quit();
	}

	public static void Log (object obj)
	{
		print(obj);
	}

	// void OnApplicationQuit ()
	// {
	// 	PlayerPrefs.DeleteAll();
	// }
		
#if UNITY_EDITOR
		public static void DestroyOnNextEditorUpdate (Object obj)
		{
			EditorApplication.update += () => { if (obj == null) return; DestroyObject (obj); };
		}

		static void DestroyObject (Object obj)
		{
			if (obj == null)
				return;
			EditorApplication.update -= () => { DestroyObject (obj); };
			DestroyImmediate(obj);
		}
#endif
}
