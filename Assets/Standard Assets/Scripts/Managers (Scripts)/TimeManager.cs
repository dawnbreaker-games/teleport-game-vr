﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TimeManager : SingletonMonoBehaviour<TimeManager>
{
	public void SetTimeScale (float timeScale)
	{
		Time.timeScale = timeScale;
	}
}
