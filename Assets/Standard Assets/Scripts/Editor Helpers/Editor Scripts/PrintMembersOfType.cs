#if UNITY_EDITOR
using System;
using UnityEngine;
using System.Reflection;

namespace VRGame
{
	[ExecuteInEditMode]
	public class PrintMembersOfType : EditorScript
	{
		public string typeName;

		public override void Do ()
		{
			_Do (typeName);
		}

		static void _Do (string typeName)
		{
			MemberInfo[] members = Type.GetType(typeName).GetMembers();
			for (int i = 0; i < members.Length; i ++)
			{
				MemberInfo member = members[i];
				print(member);
			}
		}
	}
}
#else
namespace VRGame
{
	public class PrintMembersOfType : EditorScript
	{
	}
}
#endif