#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace VRGame
{
	public class PlaceObjectOnTerrainMesh : EditorScript
	{
		public Transform trs;
		public float myHeight;
		const float MAX_HEIGHT = 99999;

		public override void Do ()
		{
			if (trs == null)
				trs = GetComponent<Transform>();
			trs.position = Vector3.up * MAX_HEIGHT;
			MakeTerrainMesh[] makeTerrainMeshes = FindObjectsOfType<MakeTerrainMesh>();
			for (int i = 0; i < makeTerrainMeshes.Length; i ++)
			{
				MakeTerrainMesh makeTerrainMesh = makeTerrainMeshes[i];
				for (int i2 = 0; i2 < makeTerrainMesh.strokesGroup.strokes.Count; i ++)
				{
					MakeTerrainMesh.Stroke stroke = makeTerrainMesh.strokesGroup.strokes[i];
					LineSegment3D lineSegment = new LineSegment3D(trs.position, trs.position + Vector3.down * (MAX_HEIGHT + float.Epsilon));
					Vector3? hitPoint = stroke.GetIntersectionPointWithLineSegment(lineSegment);
					if (hitPoint != null)
					{
						trs.position = (Vector3) hitPoint + Vector3.up * myHeight / 2;
						return;
					}
				}
			}
		}
	}
}
#else
namespace VRGame
{
	public class PlaceObjectOnTerrainMesh : EditorScript
	{
	}
}
#endif