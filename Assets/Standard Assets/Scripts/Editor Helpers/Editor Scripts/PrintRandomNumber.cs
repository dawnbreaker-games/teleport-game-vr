#if UNITY_EDITOR
using System;
using Extensions;
using UnityEngine;
using UnityEditor;
using UnityEngine.Rendering;
using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace VRGame
{
	public class PrintRandomNumber : EditorScript
	{
		public FloatRange range;

		public override void Do ()
		{
			print(range.Get(Random.value));
		}
	}
}
#else
namespace VRGame
{
	public class PrintRandomNumber : EditorScript
	{
	}
}
#endif
