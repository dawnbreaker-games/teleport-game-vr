#if UNITY_EDITOR
using Extensions;
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Formats.Alembic.Importer;

namespace VRGame
{
	[ExecuteInEditMode]
	public class ChangeAlembicFile : EditorScript
	{
        public string fileName;
		public AlembicStreamPlayer alembicStreamPlayer;

		public override void Do ()
		{
			if (alembicStreamPlayer == null)
				alembicStreamPlayer = GetComponent<AlembicStreamPlayer>();
			_Do (alembicStreamPlayer, fileName);
		}

		static void _Do (AlembicStreamPlayer alembicStreamPlayer, string fileName)
		{
            alembicStreamPlayer.LoadFromFile(fileName);
		}
	}
}
#else
namespace VRGame
{
	public class ChangeAlembicFile : EditorScript
	{
	}
}
#endif