#if UNITY_EDITOR
using System;
using Extensions;
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using Unity.EditorCoroutines.Editor;
using Random = UnityEngine.Random;

namespace VRGame
{
	[ExecuteInEditMode]
	public class MakeTerrainMesh : EditorScript
	{
		public static MakeTerrainMesh instance;
		public static MakeTerrainMesh Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<MakeTerrainMesh>();
				return instance;
			}
		}
		public float areasHeight;
		public Area[] areas = new Area[0];
		public Area currentArea;
		public float lerpDistanceBetweenAreas;
		public float checkIntervalForDistanceToAreas;
		public MeshFilter strokePrefab;
		public Transform strokesParent;
		public MakeTerrainMeshStrokeGroup strokesGroup;
		public MakeTerrainMeshStrokeGroup strokesToHugGroup;
		public int maxRetries;
		public bool shouldAddColliders;
		public bool setAreasShapesAndBounds;

		public override void OnValidate ()
		{
			instance = this;
			if (setAreasShapesAndBounds)
			{
				setAreasShapesAndBounds = false;
				for (int i = 0; i < areas.Length; i ++)
					areas[i].SetShapeAndBounds (areasHeight);
			}
			base.OnValidate ();
		}

		public override void Do ()
		{
			AnimationCurve strokeWidthCurve = new AnimationCurve();
			int keyCount;
			Keyframe[] keyframes = new Keyframe[0];
			float strokeWidth = 0;
			Vector3 startPoint = new Vector3();
			Vector3 startDirection = new Vector3();
			Stroke strokeToHug = null;
			int directionIndexAtHugPoint = 0;
			int strokeToHugIndex = 0;
			currentArea = default(Area);
			SerializableDictionary<Area, AnimationCurve> areasStrokeWidthCurvesDict = new SerializableDictionary<Area, AnimationCurve>();
			for (int i = 0; i < areas.Length; i ++)
			{
				Area area = areas[i];
				AnimationCurve strokeWidthCurve2 = new AnimationCurve();
				keyCount = area.strokeWidthKeyCountRange.Get(Random.value);
				keyframes = new Keyframe[keyCount];
				float strokeWidth2 = 0;
				if (!area.useStrokeWidthCurve)
					strokeWidth2 = area.strokeWidthRange.Get(Random.value);
				for (int i2 = 0; i2 < keyCount; i2 ++)
				{
					if (area.useStrokeWidthCurve)
						strokeWidth2 = area.strokeWidthRange.Get(Random.value);
					Keyframe keyframe = new Keyframe((float) i2 / (keyCount - 1), strokeWidth2);
					keyframes[i2] = keyframe;
				}
				strokeWidthCurve2.keys = keyframes;
				areasStrokeWidthCurvesDict.Add(area, strokeWidthCurve2);
			}
			areasStrokeWidthCurvesDict.Init ();
			List<(Area, float)> lerpAmountsToAreas = new List<(Area, float)>();
			List<Area> otherAreas = new List<Area>(areas);
			if (strokesToHugGroup.strokes.Count > 0)
			{
				int retryCount = 0;
				while (true)
				{
					strokeToHugIndex = Random.Range(0, strokesToHugGroup.strokes.Count);
					strokeToHug = strokesToHugGroup.strokes[strokeToHugIndex];
					List<Stroke.HuggableFace> huggableFaces = strokesToHugGroup.strokesHuggableFacesDict[strokeToHug];
					int huggableFaceIndex = Random.Range(0, huggableFaces.Count);
					Stroke.HuggableFace huggableFace = huggableFaces[huggableFaceIndex];
					directionIndexAtHugPoint = huggableFace.directionIndex;
					strokesToHugGroup.strokesHuggableFacesDict[strokeToHug].RemoveAt(huggableFaceIndex);
					if (strokesToHugGroup.strokesHuggableFacesDict[strokeToHug].Count == 0)
					{
						strokesToHugGroup.strokesHuggableFacesDict.Remove(strokeToHug);
						strokesToHugGroup.strokes.RemoveAt(strokeToHugIndex);
						strokeToHugIndex --;
					}
					for (int i = 0 ; i < areas.Length; i ++)
					{
						Area area = areas[i];
						Stroke.Face face = strokeToHug.faces[huggableFace.faceIndex];
						Vector3 averagePoint = face.GetAveragePoint();
						if (area.bounds.Contains(averagePoint) && area.shape.Contains(averagePoint))
						{
							currentArea = area;
							otherAreas.RemoveAt(i);
							strokeWidth = areasStrokeWidthCurvesDict[area].Evaluate(0);
							startPoint = averagePoint + face.normal * (strokeWidth / 2 + currentArea.strokeSeperation);
							startPoint.y = currentArea.GetHeight(startPoint.x, startPoint.z);
							break;
						}
					}
					float initialHeight = startPoint.y;
					float initialStrokeWidth = strokeWidth;
					for (int i = 0; i < otherAreas.Count; i ++)
					{
						Area area = otherAreas[i];
						Bounds areaBounds = area.bounds;
						areaBounds.Expand(Vector3.one * lerpDistanceBetweenAreas * 2);
						if (areaBounds.Contains(startPoint))
						{
							(Vector3 closestPoint, float distanceSqr) closestPointAndDistanceSqr = area.shape.GetClosestPointAndDistanceSqr(startPoint, checkIntervalForDistanceToAreas);
							float distanceToArea = Mathf.Sqrt(closestPointAndDistanceSqr.distanceSqr);
							if (distanceToArea < lerpDistanceBetweenAreas)
							{
								float lerpAmount = 1f - distanceToArea / lerpDistanceBetweenAreas;
								lerpAmountsToAreas.Add((area, lerpAmount));
								float strokeWidth2 = areasStrokeWidthCurvesDict[area].Evaluate(0);
								strokeWidth += Mathf.Lerp(initialStrokeWidth, strokeWidth2, lerpAmount);
								startPoint.y += Mathf.Lerp(initialHeight, area.GetHeight(startPoint.x, startPoint.z), lerpAmount);
							}
						}
					}
					strokeWidth /= lerpAmountsToAreas.Count + 1;
					startPoint.y /= lerpAmountsToAreas.Count + 1;
					startDirection = strokeToHug.directions[directionIndexAtHugPoint];
					bool shouldRetry = false;
					for (int i = 0; i < strokesToHugGroup.strokes.Count; i ++)
					{
						Stroke _stroke = strokesToHugGroup.strokes[i];
						if (i != strokeToHugIndex)
						{
							LineSegment3D lineSegment = new LineSegment3D(startPoint, startPoint + startDirection * (currentArea.strokeSampleInterval + currentArea.strokeSeperation));
							if (_stroke.bounds.Contains(lineSegment.start) || _stroke.bounds.Contains(lineSegment.end))
							{
								for (int i2 = 0; i2 < strokesToHugGroup.strokesHuggableFacesDict[_stroke].Count; i2 ++)
								{
									huggableFace = strokesToHugGroup.strokesHuggableFacesDict[_stroke][i2];
									Stroke.Face face = _stroke.faces[huggableFace.faceIndex];
									Vector3? hitPoint = face.GetIntersectionPointWithLineSegment(lineSegment);
									if (hitPoint == null)
									{
										Vector3 cross = Vector3.Cross(startDirection, Vector3.up).normalized;
										LineSegment3D lineSegment2 = new LineSegment3D(startPoint - cross * (currentArea.strokeSampleInterval + currentArea.strokeSeperation + strokeWidth / 2), startPoint + cross * (currentArea.strokeSampleInterval + currentArea.strokeSeperation + strokeWidth / 2));
										if (_stroke.bounds.Contains(lineSegment2.start) || _stroke.bounds.Contains(lineSegment2.end))
										{
											hitPoint = face.GetIntersectionPointWithLineSegment(lineSegment2);
											if (hitPoint != null)
											{
												strokesToHugGroup.strokesHuggableFacesDict[_stroke].RemoveAt(i2);
												if (strokesToHugGroup.strokesHuggableFacesDict[_stroke].Count == 0)
												{
													strokesToHugGroup.strokesHuggableFacesDict.Remove(_stroke);
													strokesToHugGroup.strokes.RemoveAt(i);
													i --;
												}
											}
											shouldRetry = true;
											break;
										}
									}
									else
									{
										strokesToHugGroup.strokesHuggableFacesDict[_stroke].RemoveAt(i2);
										if (strokesToHugGroup.strokesHuggableFacesDict[_stroke].Count == 0)
										{
											strokesToHugGroup.strokesHuggableFacesDict.Remove(_stroke);
											strokesToHugGroup.strokes.RemoveAt(i);
											i --;
										}
										shouldRetry = true;
										break;
									}
								}
							}
						}
					}
					if (!shouldRetry)
						break;
					if (retryCount > maxRetries)
						return;
					lerpAmountsToAreas.Clear();
					strokeWidth = initialStrokeWidth;
					otherAreas = new List<Area>(areas);
					retryCount ++;
				}
			}
			else
			{
				startPoint = GetComponent<Transform>().position;
				for (int i = 0 ; i < areas.Length; i ++)
				{
					Area area = areas[i];
					if (area.bounds.Contains(startPoint) && area.shape.Contains(startPoint))
					{
						currentArea = area;
						otherAreas.RemoveAt(i);
						strokeWidth = areasStrokeWidthCurvesDict[area].Evaluate(0);
						startPoint.y = currentArea.GetHeight(startPoint.x, startPoint.z);
						break;
					}
				}
				float initialHeight = startPoint.y;
				float initialStrokeWidth = strokeWidth;
				for (int i = 0; i < otherAreas.Count; i ++)
				{
					Area area = otherAreas[i];
					Bounds areaBounds = area.bounds;
					areaBounds.Expand(Vector3.one * lerpDistanceBetweenAreas * 2);
					if (areaBounds.Contains(startPoint))
					{
						(Vector3 closestPoint, float distanceSqr) closestPointAndDistanceSqr = area.shape.GetClosestPointAndDistanceSqr(startPoint, checkIntervalForDistanceToAreas);
						float distanceToArea = Mathf.Sqrt(closestPointAndDistanceSqr.distanceSqr);
						if (distanceToArea < lerpDistanceBetweenAreas)
						{
							float lerpAmount = 1f - distanceToArea / lerpDistanceBetweenAreas;
							lerpAmountsToAreas.Add((area, lerpAmount));
							float strokeWidth2 = areasStrokeWidthCurvesDict[area].Evaluate(0);
							strokeWidth += Mathf.Lerp(initialStrokeWidth, strokeWidth2, lerpAmount);
							startPoint.y += Mathf.Lerp(initialHeight, area.GetHeight(startPoint.x, startPoint.z), lerpAmount);
						}
					}
				}
				strokeWidth /= lerpAmountsToAreas.Count + 1;
				startPoint.y /= lerpAmountsToAreas.Count + 1;
				startDirection = Random.onUnitSphere;
			}
			Stroke.ControlPoint[] controlPoints = new Stroke.ControlPoint[currentArea.controlPointCountRange.Get(Random.value)];
			for (int i = 0; i < controlPoints.Length; i ++)
			{
				Stroke.ControlPoint controlPoint = new Stroke.ControlPoint();
				controlPoint.point = startPoint + (Random.insideUnitCircle.normalized * currentArea.maxStrokeControlPointDisatanceFromStart).XYToXZ();
				controlPoint.startUsingAtNormalizedLength = (float) i / controlPoints.Length;
				controlPoints[i] = controlPoint;
			}
			SerializableDictionary<Area, AnimationCurve> areasStrokeTurnRateCurvesDict = new SerializableDictionary<Area, AnimationCurve>();
			for (int i = 0; i < areas.Length; i ++)
			{
				Area area = areas[i];
				AnimationCurve strokeTurnRateCurve = new AnimationCurve();
				keyCount = area.strokeTurnRateKeyCountRange.Get(Random.value);
				keyframes = new Keyframe[keyCount];
				float strokeTurnRate = 0;
				if (!area.useStrokeTurnRateCurve)
					strokeTurnRate = area.strokeTurnRateRange.Get(Random.value);
				for (int i2 = 0; i2 < keyCount; i2 ++)
				{
					if (area.useStrokeTurnRateCurve)
						strokeTurnRate = area.strokeTurnRateRange.Get(Random.value);
					Keyframe keyframe = new Keyframe((float) i2 / (keyCount - 1), strokeTurnRate);
					keyframes[i2] = keyframe;
				}
				strokeTurnRateCurve.keys = keyframes;
				areasStrokeTurnRateCurvesDict.Add(area, strokeTurnRateCurve);
			}
			SerializableDictionary<Area, Gradient> areasStrokeColorGradientsDict = new SerializableDictionary<Area, Gradient>();
			for (int i = 0; i < areas.Length; i ++)
			{
				Area area = areas[i];
				Gradient strokeColorGradient = new Gradient();
				keyCount = area.strokeColorKeyCountRange.Get(Random.value);
				GradientColorKey[] colorKeys = new GradientColorKey[keyCount];
				Color strokeColor = new Color();
				if (!area.useStrokeColorCurve)
					strokeColor = area.strokeColorGradient.Evaluate(Random.value);
				for (int i2 = 0; i2 < keyCount; i2 ++)
				{
					if (area.useStrokeColorCurve)
						strokeColor = area.strokeColorGradient.Evaluate(Random.value);
					GradientColorKey colorKey = new GradientColorKey(strokeColor, (float) i2 / (keyCount - 1));
					colorKeys[i2] = colorKey;
				}
				strokeColorGradient.colorKeys = colorKeys;
				areasStrokeColorGradientsDict.Add(area, strokeColorGradient);
			}
			float strokeLength = currentArea.strokeLengthRange.Get(Random.value);
			float strokeSampleInterval = currentArea.strokeSampleInterval;
			int strokeCornersPerRing = currentArea.strokeCornersPerRing;
			bool goBackwardsAlongHuggedStroke = false;
			if (Random.value < 0.5f)
				goBackwardsAlongHuggedStroke = true;
			Stroke stroke = new Stroke(this, strokeToHug, strokeToHugIndex, goBackwardsAlongHuggedStroke, directionIndexAtHugPoint, startPoint, startDirection, controlPoints, areasStrokeTurnRateCurvesDict, areasStrokeWidthCurvesDict, areasStrokeColorGradientsDict, strokeLength, strokeSampleInterval, strokeCornersPerRing);
			if (stroke.corners.Length > 0)
			{
				MeshFilter newMeshFilter = Instantiate(strokePrefab, strokesParent);
				newMeshFilter.mesh = stroke.ToMesh();
				if (shouldAddColliders)
					newMeshFilter.gameObject.AddComponent<MeshCollider>();
				else
					newMeshFilter.name += " [No collider]";
				strokesGroup.strokes.Add(stroke);
				strokesToHugGroup.strokes.Add(stroke);
				MakeTerrainMeshStrokeGroup.HuggableFaceGroup huggableFaces = new MakeTerrainMeshStrokeGroup.HuggableFaceGroup();
				for (int i = stroke.cornersPerRing; i < stroke.faces.Length - stroke.cornersPerRing - 1; i ++)
				{
					Stroke.Face face = stroke.faces[i];
					Stroke.HuggableFace huggableFace = new Stroke.HuggableFace();
					huggableFace.faceIndex = i;
					huggableFace.directionIndex = (int) ((float) i / stroke.faces.Length * stroke.directions.Length);
					huggableFaces.Add(huggableFace);
				}
				strokesToHugGroup.strokesHuggableFacesDict.Add(stroke, huggableFaces);
			}
		}

		[MenuItem("Game/Stop MakeTerrainMesh")]
		static void Stop ()
		{
			Instance.doRepeatedly = false;
			instance.OnValidate ();
		}

		[MenuItem("Game/Clear strokes for MakeTerrainMesh")]
		static void ClearStrokes ()
		{
			Instance.strokesGroup.strokes.Clear();
			instance.strokesGroup.strokesHuggableFacesDict.Clear();
		}

		[MenuItem("Game/Clear strokes to hug for MakeTerrainMesh")]
		static void ClearStrokesToHug ()
		{
			Instance.strokesToHugGroup.strokes.Clear();
			instance.strokesToHugGroup.strokesHuggableFacesDict.Clear();
		}

		[MenuItem("Game/Begin or continue MakeTerrainMesh")]
		static void BeginOrContinue ()
		{
			Instance.doRepeatedly = true;
			instance.OnValidate ();
		}

		[Serializable]
		public class Stroke : global::Stroke
		{
			public MakeTerrainMesh creator;
			public Stroke strokeToHug;
			public int strokeToHugIndex;
			public int huggedSampleCount;
			public bool goBackwardsAlongHuggedStroke;
			public Vector3[] positions = new Vector3[0];
			public Vector3[] directions = new Vector3[0];
			public int directionIndexAtHugPoint;
			public Bounds bounds;
			public SerializableDictionary<Area, AnimationCurve> areasStrokeTurnRateCurvesDict = new SerializableDictionary<Area, AnimationCurve>();
			public SerializableDictionary<Area, AnimationCurve> areasStrokeWidthCurvesDict = new SerializableDictionary<Area, AnimationCurve>();
			public SerializableDictionary<Area, Gradient> areasStrokeColorGradientsDict = new SerializableDictionary<Area, Gradient>();

			public Stroke ()
			{
			}

			public Stroke (MakeTerrainMesh creator, Stroke strokeToHug, int strokeToHugIndex, bool goBackwardsAlongHuggedStroke, int directionIndexAtHugPoint, Vector3 startPoint, Vector3 startDirection, ControlPoint[] controlPoints, SerializableDictionary<Area, AnimationCurve> areasStrokeTurnRateCurvesDict, SerializableDictionary<Area, AnimationCurve> areasStrokeWidthCurvesDict, SerializableDictionary<Area, Gradient> areasStrokeColorGradientsDict, float length, float sampleInterval, int cornersPerRing)
			{
				this.creator = creator;
				this.strokeToHug = strokeToHug;
				this.strokeToHugIndex = strokeToHugIndex;
				this.goBackwardsAlongHuggedStroke = goBackwardsAlongHuggedStroke;
				this.directionIndexAtHugPoint = directionIndexAtHugPoint;
				this.startPoint = startPoint;
				this.startDirection = startDirection;
				this.controlPoints = controlPoints;
				this.areasStrokeTurnRateCurvesDict = areasStrokeTurnRateCurvesDict;
				turnRateCurve = areasStrokeTurnRateCurvesDict[creator.currentArea];
				this.areasStrokeWidthCurvesDict = areasStrokeWidthCurvesDict;
				widthCurve = areasStrokeWidthCurvesDict[creator.currentArea];
				this.areasStrokeColorGradientsDict = areasStrokeColorGradientsDict;
				colorGradient = areasStrokeColorGradientsDict[creator.currentArea];
				this.length = length;
				this.sampleInterval = sampleInterval;
				this.cornersPerRing = cornersPerRing;
				SetCornersAndEdgesAndFaces ();
			}

			public override void SetCornersAndEdgesAndFaces ()
			{
				SetCorners ();
				if (corners.Length == 0)
					return;
				SetFacesFromCorners ();
				SetEdgesFromFaces ();
			}

			public override void SetCorners ()
			{
				float minWidth = Mathf.Infinity;
				float maxWidth = -Mathf.Infinity;
				float minHeight = Mathf.Infinity;
				float maxHeight = -Mathf.Infinity;
				float minDepth = Mathf.Infinity;
				float maxDepth = -Mathf.Infinity;
				float lengthTraveled = 0;
				float normalizedLengthTraveled = lengthTraveled / length;
				List<Vector3> corners = new List<Vector3>();
				List<Vector3> directions = new List<Vector3>();
				List<Vector3> positions = new List<Vector3>();
				Vector3 forward = startDirection;
				Vector3 position = startPoint;
				int controlPointIndex = 0;
				ControlPoint controlPoint = controlPoints[0];
				ControlPoint nextControlPoint = null;
				if (controlPoints.Length > 1)
					nextControlPoint = controlPoints[1];
				int retryCount = 0;
				List<Area> otherAreas = new List<Area>(creator.areas);
				otherAreas.Remove(creator.currentArea);
				while (true)
				{
					List<(Area, float)> lerpAmountsToAreas = new List<(Area, float)>();
					Vector3? hitPoint = null;
					float radius = widthCurve.Evaluate(normalizedLengthTraveled) / 2;
					float turnRate = turnRateCurve.Evaluate(normalizedLengthTraveled);
					float initialTurnRate = turnRate;
					float initialRadius = radius;
					for (int i = 0; i < otherAreas.Count; i ++)
					{
						Area area = otherAreas[i];
						Bounds areaBounds = area.bounds;
						areaBounds.Expand(Vector3.one * creator.lerpDistanceBetweenAreas * 2);
						if (areaBounds.Contains(position))
						{
							(Vector3 closestPoint, float distanceSqr) closestPointAndDistanceSqr = area.shape.GetClosestPointAndDistanceSqr(position, creator.checkIntervalForDistanceToAreas);
							float distanceToArea = Mathf.Sqrt(closestPointAndDistanceSqr.distanceSqr);
							if (distanceToArea < creator.lerpDistanceBetweenAreas)
							{
								float lerpAmount = 1f - distanceToArea / creator.lerpDistanceBetweenAreas;
								lerpAmountsToAreas.Add((area, lerpAmount));
								float turnRate2 = areasStrokeTurnRateCurvesDict[area].Evaluate(normalizedLengthTraveled);
								float radius2 = areasStrokeWidthCurvesDict[area].Evaluate(normalizedLengthTraveled) / 2;
								turnRate += Mathf.Lerp(initialTurnRate, turnRate2, lerpAmount);
								radius += Mathf.Lerp(initialRadius, radius2, lerpAmount);
							}
						}
					}
					turnRate /= lerpAmountsToAreas.Count + 1;
					radius /= lerpAmountsToAreas.Count + 1;
					Vector3 tangent = Vector3.up;
					Vector3.OrthoNormalize(ref forward, ref tangent);
					for (int i = 0; i < cornersPerRing; i ++)
					{
						Vector3 corner = position + tangent * radius;
						corner = corner.Rotate(position, Quaternion.AngleAxis(360f / cornersPerRing * i, forward));
						corners.Add(corner);
					}
					positions.Add(position);
					directions.Add(forward);
					if (strokeToHug == null || Random.value < creator.currentArea.strokeChanceToStopHug)
							forward = Vector3.RotateTowards(forward, controlPoint.point - position, turnRate * sampleInterval * Mathf.Deg2Rad, 0);
					if (strokeToHug == null && Random.value < 0.5f)
						goBackwardsAlongHuggedStroke = !goBackwardsAlongHuggedStroke;
					strokeToHug = null;
					float closestHitPointDistanceSqr = Mathf.Infinity;
					bool closestHitPointHitBecuaseOfWidth = false;
					if (creator.strokesToHugGroup.strokes.Count > 1)
					{
						int closestStrokeHuggedSampleCount = 0;
						LineSegment3D lineSegment = new LineSegment3D(position, position + forward * (sampleInterval + creator.currentArea.strokeSeperation));
						for (int i = 0; i < creator.strokesToHugGroup.strokes.Count; i ++)
						{
							Stroke stroke = creator.strokesToHugGroup.strokes[i];
							int huggedSampleCount = 0;
							bool hitBecauseOfWidth = false;
							for (int i2 = 0; i2 < stroke.faces.Length; i2 ++)
							{
								hitPoint = null;
								Face face = stroke.faces[i2];
								if (stroke.bounds.Contains(lineSegment.start) || stroke.bounds.Contains(lineSegment.end))
									hitPoint = face.GetIntersectionPointWithLineSegment(lineSegment);
								if (hitPoint == null)
								{
									Vector3 endPosition = position + forward * sampleInterval;
									Vector3 cross = Vector3.Cross(forward, Vector3.up).normalized;
									LineSegment3D lineSegment2 = new LineSegment3D(endPosition - cross * (radius + creator.currentArea.strokeSeperation), endPosition + cross * (radius * creator.currentArea.strokeSeperation));
									if (stroke.bounds.Contains(lineSegment2.start) || stroke.bounds.Contains(lineSegment2.end))
									{
										hitPoint = face.GetIntersectionPointWithLineSegment(lineSegment2);
										if (hitPoint != null)
											hitBecauseOfWidth = true;
									}
								}
								if (hitPoint != null)
								{
									float hitPointDistanceSqr = (position - (Vector3) hitPoint).sqrMagnitude;
									huggedSampleCount ++;
									if (hitPointDistanceSqr < closestHitPointDistanceSqr)
									{
										closestHitPointDistanceSqr = hitPointDistanceSqr;
										closestStrokeHuggedSampleCount = huggedSampleCount;
										closestHitPointHitBecuaseOfWidth = hitBecauseOfWidth;
										strokeToHug = stroke;
										strokeToHugIndex = i;
										directionIndexAtHugPoint = (int) ((float) i2 / stroke.faces.Length * stroke.directions.Length);
										directionIndexAtHugPoint = Mathf.Clamp(directionIndexAtHugPoint, 0, stroke.directions.Length - 1);
									}
									break;
								}
							}
						}
						if (strokeToHug != null)
						{
							forward = strokeToHug.directions[directionIndexAtHugPoint];
							if (goBackwardsAlongHuggedStroke)
								forward *= -1;
							if (directionIndexAtHugPoint == 0 || directionIndexAtHugPoint == strokeToHug.directions.Length - 1)
								forward = Quaternion.Euler(0, 90, 0) * forward;
							strokeToHug.huggedSampleCount += closestStrokeHuggedSampleCount;
							huggedSampleCount += closestStrokeHuggedSampleCount;
							MakeTerrainMeshStrokeGroup.HuggableFaceGroup huggableFaces = creator.strokesToHugGroup.strokesHuggableFacesDict[strokeToHug];
							for (int i = 0; i < huggableFaces.Count; i ++)
							{
								HuggableFace huggableFace = huggableFaces[i];
								if (huggableFace.directionIndex == directionIndexAtHugPoint)
								{
									creator.strokesToHugGroup.strokesHuggableFacesDict[strokeToHug].RemoveAt(i);
									if (creator.strokesToHugGroup.strokesHuggableFacesDict[strokeToHug].Count == 0)
									{
										creator.strokesToHugGroup.strokes.RemoveAt(strokeToHugIndex);
										creator.strokesToHugGroup.strokesHuggableFacesDict.Remove(strokeToHug);
										strokeToHugIndex --;
									}
									break;
								}
							}
						}
					}
					lengthTraveled += sampleInterval;
					normalizedLengthTraveled = lengthTraveled / length;
					if (nextControlPoint != null && normalizedLengthTraveled >= nextControlPoint.startUsingAtNormalizedLength)
					{
						controlPoint = nextControlPoint;
						controlPointIndex ++;
						if (controlPoints.Length > controlPointIndex)
							nextControlPoint = controlPoints[controlPointIndex];
					}
					if (lengthTraveled > length)
					{
						if (lengthTraveled >= length + sampleInterval)
							break;
						else
						{
							if (retryCount > creator.maxRetries)
								return;
							retryCount ++;
						}
						if (hitPoint != null)
						{
							if (closestHitPointHitBecuaseOfWidth)
								position += Vector3.ClampMagnitude(forward * (sampleInterval - (lengthTraveled - length)), Mathf.Sqrt(closestHitPointDistanceSqr) - radius - creator.currentArea.strokeSeperation);
							else
								position += Vector3.ClampMagnitude(forward * (sampleInterval - (lengthTraveled - length)), Mathf.Sqrt(closestHitPointDistanceSqr) - creator.currentArea.strokeSeperation);
						}
						else
							position += forward * (sampleInterval - (lengthTraveled - length));
						lengthTraveled = length;
						normalizedLengthTraveled = 1;
					}
					else
					{
						if (position.x - radius < minWidth)
							minWidth = position.x - radius;
						if (position.x + radius > maxWidth)
							maxWidth = position.x + radius;
						if (position.y - radius < minHeight)
							minHeight = position.y - radius;
						if (position.y + radius > maxHeight)
							maxHeight = position.y + radius;
						if (position.z - radius < minDepth)
							minDepth = position.z - radius;
						if (position.z + radius > maxDepth)
							maxDepth = position.z + radius;
						if (hitPoint != null)
						{
							if (closestHitPointHitBecuaseOfWidth)
								position += Vector3.ClampMagnitude(forward * sampleInterval, Mathf.Sqrt(closestHitPointDistanceSqr) - radius - creator.currentArea.strokeSeperation);
							else
								position += Vector3.ClampMagnitude(forward * sampleInterval, Mathf.Sqrt(closestHitPointDistanceSqr) - creator.currentArea.strokeSeperation);
						}
						else
							position += forward * sampleInterval;
					}
					position.y = creator.currentArea.GetHeight(position.x, position.z);
					float initialHeight = position.y;
					lerpAmountsToAreas.Clear();
					for (int i = 0; i < otherAreas.Count; i ++)
					{
						Area area = otherAreas[i];
						Bounds areaBounds = area.bounds;
						areaBounds.Expand(Vector3.one * creator.lerpDistanceBetweenAreas * 2);
						if (areaBounds.Contains(position))
						{
							(Vector3 closestPoint, float distanceSqr) closestPointAndDistanceSqr = area.shape.GetClosestPointAndDistanceSqr(position, creator.checkIntervalForDistanceToAreas);
							float distanceToArea = Mathf.Sqrt(closestPointAndDistanceSqr.distanceSqr);
							if (distanceToArea < creator.lerpDistanceBetweenAreas)
							{
								float lerpAmount = 1f - distanceToArea / creator.lerpDistanceBetweenAreas;
								lerpAmountsToAreas.Add((area, lerpAmount));
								float height = area.GetHeight(position.x, position.z);
								position.y += Mathf.Lerp(initialHeight, height, lerpAmount);
							}
						}
					}
					position.y /= lerpAmountsToAreas.Count + 1;
				}
				this.corners = corners.ToArray();
				this.positions = positions.ToArray();
				this.directions = directions.ToArray();
				bounds = new Bounds();
				bounds.SetMinMax(new Vector3(minWidth, minHeight, minDepth), new Vector3(maxWidth, maxHeight, maxDepth));
			}

			public override Color[] GetColors (Vector3[] vertices)
			{
				Color[] colors = new Color[vertices.Length];
				for (int i = 0; i < colors.Length; i ++)
				{
					Color color = colorGradient.Evaluate((float) i / (colors.Length - 1));
					Color initialColor = color;
					List<(Area, float)> lerpAmountsToAreas = new List<(Area, float)>();
					List<Area> otherAreas = new List<Area>(creator.areas);
					otherAreas.Remove(creator.currentArea);
					Vector3 vertex = vertices[i];
					for (int i2 = 0; i2 < otherAreas.Count; i2 ++)
					{
						Area area = otherAreas[i2];
						Bounds areaBounds = area.bounds;
						areaBounds.Expand(Vector3.one * creator.lerpDistanceBetweenAreas * 2);
						if (areaBounds.Contains(vertex))
						{
							(Vector3 closestPoint, float distanceSqr) closestPointAndDistanceSqr = area.shape.GetClosestPointAndDistanceSqr(vertex, creator.checkIntervalForDistanceToAreas);
							float distanceToArea = Mathf.Sqrt(closestPointAndDistanceSqr.distanceSqr);
							if (distanceToArea < creator.lerpDistanceBetweenAreas)
							{
								float lerpAmount = 1f - distanceToArea / creator.lerpDistanceBetweenAreas;
								lerpAmountsToAreas.Add((area, lerpAmount));
								Color color2 = areasStrokeColorGradientsDict[area].Evaluate((float) i / (colors.Length - 1));
								color += Color.Lerp(initialColor, color2, lerpAmount);
							}
						}
					}
					color /= lerpAmountsToAreas.Count + 1;
					colors[i] = color;
				}
				return colors;
			}

			public Stroke Move (Vector3 movement)
			{
				Stroke output = new Stroke();
				output.corners = new Vector3[corners.Length];
				for (int i = 0; i < corners.Length; i ++)
					output.corners[i] = corners[i] + movement;
				output.SetFacesFromCorners ();
				output.SetEdgesFromFaces ();
				output.startPoint += movement;
				output.areasStrokeWidthCurvesDict = areasStrokeWidthCurvesDict;
				output.widthCurve = widthCurve;
				output.areasStrokeTurnRateCurvesDict = areasStrokeTurnRateCurvesDict;
				output.turnRateCurve = turnRateCurve;
				output.length = length;
				output.areasStrokeColorGradientsDict = areasStrokeColorGradientsDict;
				output.colorGradient = colorGradient;
				output.sampleInterval = sampleInterval;
				output.cornersPerRing = cornersPerRing;
				output.controlPoints = new ControlPoint[controlPoints.Length];
				for (int i = 0; i < controlPoints.Length; i ++)
				{
					ControlPoint controlPoint = controlPoints[i];
					controlPoint.point += movement;
					output.controlPoints[i] = controlPoint;
				}
				output.creator = creator;
				output.strokeToHug = strokeToHug;
				output.strokeToHugIndex = strokeToHugIndex;
				output.huggedSampleCount = huggedSampleCount;
				output.positions = new Vector3[positions.Length];
				for (int i = 0; i < positions.Length; i ++)
					output.positions[i] = positions[i] + movement;
				output.directions = directions;
				output.directionIndexAtHugPoint = directionIndexAtHugPoint;
				output.bounds = new Bounds();
				output.bounds.SetMinMax(bounds.min + movement, bounds.max + movement);
				return output;
			}

			[Serializable]
			public struct HuggableFace
			{
				public int faceIndex;
				public int directionIndex;
			}
		}

		[Serializable]
		public struct Area
		{
			public IntRange controlPointCountRange;
			public FloatRange strokeTurnRateRange;
			public FloatRange strokeWidthRange;
			public FloatRange strokeLengthRange;
			public bool useStrokeTurnRateCurve;
			public IntRange strokeTurnRateKeyCountRange;
			public bool useStrokeWidthCurve;
			public IntRange strokeWidthKeyCountRange;
			public bool useStrokeColorCurve;
			public IntRange strokeColorKeyCountRange;
			public float strokeSampleInterval;
			public int strokeCornersPerRing;
			public float maxStrokeControlPointDisatanceFromStart;
			public float strokeSeperation;
			// [Range(0, 2)]
			public float strokeHuggedSampleFractionTillRemoveFromStrokes;
			[Range(0, 1)]
			public float strokeChanceToStopHug;
			public Vector2 perlinNoiseOffset;
			public Vector2 perlinNoise2Offset;
			public float perlinNoiseMultiplier;
			public float perlinNoise2Multiplier;
			public float perlinNoiseVolativity;
			public float perlinNoise2Volativity;
			public float addToHeight;
			public Gradient strokeColorGradient;
			public Transform[] cornersInRingTransforms;
			public Bounds bounds;
			public Shape3D shape;

			public void SetShapeAndBounds (float height)
			{
				List<Shape3D.Face> faces = new List<Shape3D.Face>();
				int cornersPerRing = cornersInRingTransforms.Length;
				Vector3[] bottomCorners = new Vector3[cornersPerRing];
				Vector3[] topCorners = new Vector3[cornersPerRing];
				Vector3 cornerInRing = cornersInRingTransforms[cornersPerRing - 1].position;
				Vector3 previousBottomCorner = cornerInRing.SetY(-height / 2);
				Vector3 previousTopCorner = cornerInRing.SetY(height / 2);
				cornerInRing = cornersInRingTransforms[0].position;
				Vector3 bottomCorner = cornerInRing.SetY(-height / 2);
				bottomCorners[0] = bottomCorner;
				Vector3 topCorner = cornerInRing.SetY(height / 2);
				topCorners[0] = topCorner;
				faces.Add(new Shape3D.Face(new Vector3[] { previousBottomCorner, bottomCorner, topCorner, previousTopCorner } ));
				float minX = topCorner.x;
				float minZ = topCorner.z;
				float maxX = topCorner.x;
				float maxZ = topCorner.z;
				for (int i = 1; i < cornersPerRing; i ++)
				{
					cornerInRing = cornersInRingTransforms[i].position;
					previousBottomCorner = bottomCorner;
					bottomCorner = cornerInRing.SetY(-height / 2);
					bottomCorners[i] = bottomCorner;
					previousTopCorner = topCorner;
					topCorner = cornerInRing.SetY(height / 2);
					topCorners[i] = topCorner;
					faces.Add(new Shape3D.Face(new Vector3[] { previousBottomCorner, bottomCorner, topCorner, previousTopCorner } ));
					if (topCorner.x < minX)
						minX = topCorner.x;
					else if (topCorner.x > maxX)
						maxX = topCorner.x;
					if (topCorner.z < minZ)
						minZ = topCorner.z;
					else if (topCorner.z > maxZ)
						maxZ = topCorner.z;
				}
				faces.Add(new Shape3D.Face(bottomCorners));
				faces.Add(new Shape3D.Face(topCorners));
				shape = new Shape3D(faces.ToArray());
				bounds = new Bounds();
				bounds.SetMinMax(new Vector3(minX, -height / 2, minZ), new Vector3(maxX, height / 2, maxZ));
			}

			public float GetHeight (float x, float z)
			{
				return Mathf.PerlinNoise(x * perlinNoiseVolativity + perlinNoiseOffset.x, z * perlinNoiseVolativity + perlinNoiseOffset.y) * perlinNoiseMultiplier + Mathf.PerlinNoise(x * perlinNoise2Volativity + perlinNoise2Offset.x, z * perlinNoise2Volativity + perlinNoise2Offset.y) * perlinNoise2Multiplier + addToHeight;
			}
		}
	}
}
#else
namespace VRGame
{
	public class MakeTerrainMesh : EditorScript
	{
	}
}
#endif