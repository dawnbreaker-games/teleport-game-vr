#if UNITY_EDITOR
using System;
using Extensions;
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

namespace VRGame
{
	[ExecuteInEditMode]
	public class MakeTerrainMeshStrokeGroup : EditorScript
	{
		public List<MakeTerrainMesh.Stroke> strokes = new List<MakeTerrainMesh.Stroke>();
		public SerializableDictionary<MakeTerrainMesh.Stroke, HuggableFaceGroup> strokesHuggableFacesDict = new SerializableDictionary<MakeTerrainMesh.Stroke, HuggableFaceGroup>();

		[Serializable]
		public class HuggableFaceGroup : List<MakeTerrainMesh.Stroke.HuggableFace>
		{
		}
	}
}
#else
namespace VRGame
{
	public class MakeTerrainMeshStrokeGroup : EditorScript
	{
	}
}
#endif