using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace VRGame
{
	public class WebRenderer3D : EditorScript
	{
		public bool autoSetRegions;
		public Transform lineRenderersParent;
		public bool autoSetPoints;
		public Transform pointsParent;
		public Transform[] points = new Transform[0];
		public Region[] regions = new Region[0];

		public override void Do ()
		{
			if (autoSetRegions)
			{
				LineRenderer[] lineRenderers = lineRenderersParent.GetComponentsInChildren<LineRenderer>();
				regions = new Region[lineRenderers.Length]; 
				for (int i = 0; i < lineRenderers.Length; i ++)
				{
					Region region = new Region();
					region.lineRenderer = lineRenderers[i];
					region.autoSetPositions = true;
					regions[i] = region;
				}
			}
			if (autoSetPoints)
			{
				points = new Transform[pointsParent.childCount];
				for (int i = 0; i < pointsParent.childCount; i ++)
					points[i] = pointsParent.GetChild(i);
			}
			for (int i = 0; i < regions.Length; i ++)
			{
				Region region = regions[i];
				if (region.autoSetPositions)
				{
					for (int i2 = 0; i2 < region.lineRenderer.positionCount; i2 ++)
					{
						Vector3 point;
						if (region.lineRenderer.useWorldSpace)
							point = TransformExtensions.GetClosestTransform(points, region.lineRenderer.GetPosition(i2)).position;
						else
						{
							Transform trs = region.lineRenderer.GetComponent<Transform>();
							point = TransformExtensions.GetClosestTransform(points, trs.TransformPoint(region.lineRenderer.GetPosition(i2))).position;
							point = trs.InverseTransformPoint(point);
						}
						region.lineRenderer.SetPosition(i2, point);
					}
				}
			}
		}

		[Serializable]
		public class Region
		{
			public LineRenderer lineRenderer;
			public bool autoSetPositions;
		}
	}
}