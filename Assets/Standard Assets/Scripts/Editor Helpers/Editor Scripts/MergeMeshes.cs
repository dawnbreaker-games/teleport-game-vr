#if UNITY_EDITOR
using System;
using Extensions;
using UnityEngine;
using UnityEditor;
using UnityEngine.Rendering;
using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace VRGame
{
	public class MergeMeshes : EditorScript
	{
		public MeshFilter mergeTo;
		public MeshFilter[] meshFilters = new MeshFilter[0];
		public bool useSharedMeshes;
		public float detectSameVertexRange;

		public override void Do ()
		{
			_Do (meshFilters, mergeTo, useSharedMeshes, detectSameVertexRange);
		}

		static void _Do (MeshFilter[] meshFilters, MeshFilter mergeTo, bool useSharedMeshes, float detectSameVertexRange)
		{
			// Dictionary<Material, List<MeshData>> materialsForMeshesDict = new Dictionary<Material, List<MeshData>>();
			// Mesh mesh;
			// MeshRenderer meshRenderer;
			// Transform trs;
			// List<Vector3> vertices = new List<Vector3>();
			// List<Vector2> uvs = new List<Vector2>();
			// List<MeshData> meshDatas = new List<MeshData>();
			// for (int i = 0; i < meshFilters.Length; i ++)
			// {
			// 	MeshFilter meshFilter = meshFilters[i];
			// 	if (useSharedMeshes)
			// 		mesh = meshFilter.sharedMesh;
			// 	else
			// 		mesh = meshFilter.mesh;
			// 	MeshData meshData = new MeshData();
			// 	trs = meshFilter.GetComponent<Transform>();
			// 	List<Vector3> _vertices = new List<Vector3>();
			// 	mesh.GetVertices(_vertices);
			//     for (int i2 = 0; i2 < _vertices.Count; i2 ++)
			//         _vertices[i] = trs.TransformPoint(_vertices[i2]);
			// 	vertices.AddRange(_vertices);
			// 	List<Vector2> _uvs = new List<Vector2>();
			// 	mesh.GetUVs(0, _uvs);
			// 	uvs.AddRange(_uvs);
			// 	meshRenderer = meshFilter.GetComponent<MeshRenderer>();
			// 	if (meshRenderer != null)
			// 	{
			// 		Material material = meshRenderer.material;
			// 		List<MeshData> _meshDatas = new List<MeshData>();
			// 		MeshData _meshData = new MeshData(mesh, trs);
			// 		if (!materialsForMeshesDict.TryGetValue(material, out _meshDatas))
			// 			materialsForMeshesDict.Add(material, new List<MeshData>(new MeshData[] { _meshData }));
			// 		else
			// 			materialsForMeshesDict[material].Add(_meshData);
			// 	}
			// 	meshDatas.Add(meshData);
			// 	if (mergeTo != meshFilter)
			// 		GameManager.DestroyOnNextEditorUpdate (meshFilter.gameObject);
			// }
			// mesh = new Mesh();
			// mesh.SetVertices(vertices);
			// mesh.SetUVs(0, uvs);
			// mesh.subMeshCount = meshFilters.Length;
			// trs = mergeTo.GetComponent<Transform>();
			// trs.position = Vector3.zero;
			// trs.SetWorldScale (Vector3.one);
			// int _i = 0;
			// List<Material> materials = new List<Material>();
			// foreach (KeyValuePair<Material, List<MeshData>> keyValuePair in materialsForMeshesDict)
			// {
			// 	for (int i = 0; i < keyValuePair.Value.Count; i ++)
			// 	{
			// 		MeshData meshData = keyValuePair.Value[i];
			// 		Mesh _mesh = meshData.mesh;
			// 		Transform _trs = meshData.trs;
			// 		Dictionary<int, int> indicesDict = new Dictionary<int, int>();
			// 		int[] indices = _mesh.GetTriangles(0);
			// 		List<Vector3> _vertices = new List<Vector3>();
			// 		_mesh.GetVertices(_vertices);
			// 		for (int i2 = 0; i2 < indices.Length; i2 ++)
			// 		{
			// 			int index = indices[i2];
			// 			if (!indicesDict.TryGetValue(index, out indices[i2]))
			// 			{
			// 				for (int i3 = 0; i3 < vertices.Count; i3 ++)
			// 				{
			// 					Vector3 vertex = vertices[i3];
			// 					vertex = trs.TransformPoint(vertex);
			// 					Vector3 vertex2 = _vertices[index];
			// 					vertex2 = _trs.TransformPoint(vertex2);
			// 					if ((vertex - vertex2).sqrMagnitude <= detectSameVertexRange * detectSameVertexRange)
			// 					{
			// 						indicesDict[index] = i3;
			// 						indices[i2] = i3;
			// 						break;
			// 					}
			// 				}
			// 			}
			// 		}
			// 		mesh.SetTriangles(indices, _i);
			// 		materials.Add(keyValuePair.Key);
			// 		_i ++;
			// 	}
			// }
			// if (useSharedMeshes)
			// 	mergeTo.sharedMesh = mesh;
			// else
			// 	mergeTo.mesh = mesh;
			// meshRenderer = mergeTo.GetComponent<MeshRenderer>();
			// if (meshRenderer != null)
			// 	meshRenderer.sharedMaterials = materials.ToArray();
			CombineInstance[] combineInstances = new CombineInstance[meshFilters.Length];
			for (int i = 0; i < combineInstances.Length; i ++)
			{
				CombineInstance combineInstance = combineInstances[i];
				MeshFilter meshFilter = meshFilters[i];
				if (useSharedMeshes)
					combineInstance.mesh = meshFilter.sharedMesh;
				else
					combineInstance.mesh = meshFilter.mesh;
				Transform trs = meshFilter.GetComponent<Transform>();
				combineInstance.transform = trs.localToWorldMatrix;
				combineInstances[i] = combineInstance;
			}
			Mesh mergeToMesh;
			if (useSharedMeshes)
			{
				mergeToMesh = mergeTo.sharedMesh;
				// mergeTo.sharedMesh = new Mesh();
			}
			else
			{
				mergeToMesh = mergeTo.mesh;
				// mergeTo.mesh = new Mesh();
			}
			mergeToMesh.indexFormat = IndexFormat.UInt32;
			mergeToMesh.CombineMeshes(combineInstances);
		}

		[MenuItem("Tools/Merge selected shared Meshes")]
		static void _DoToSelectedSharedMeshes ()
		{
			MeshFilter[] meshFilters = SelectionExtensions.GetSelected<MeshFilter>();
			// _Do (meshFilters, meshFilters[0], true, 0.1f);
			_Do (meshFilters, new GameObject().AddComponent<MeshFilter>(), true, 0.1f);
		}
		
		[MenuItem("Tools/Merge selected Meshes")]
		static void _DoToSelectedMeshes ()
		{
			MeshFilter[] meshFilters = SelectionExtensions.GetSelected<MeshFilter>();
			// _Do (meshFilters, meshFilters[0], false, 0.1f);
			_Do (meshFilters, new GameObject().AddComponent<MeshFilter>(), false, 0.1f);
		}

		struct MeshData
		{
			public Mesh mesh;
			public Transform trs;
			public Vector2[] uvs;

			public MeshData (Mesh mesh, Transform trs, Vector2[] uvs)
			{
				this.mesh = mesh;
				this.trs = trs;
				this.uvs = uvs;
			}

			public MeshData (Mesh mesh, Transform trs)
			{
				this.mesh = mesh;
				this.trs = trs;
				List<Vector2> uvs = new List<Vector2>();
				mesh.GetUVs(0, uvs);
				this.uvs = uvs.ToArray();
			}
		}
	}
}
#else
namespace VRGame
{
	public class MergeMeshes : EditorScript
	{
	}
}
#endif
