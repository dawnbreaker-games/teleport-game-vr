#if UNITY_EDITOR
using Extensions;
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using Unity.EditorCoroutines.Editor;
using UnityEngine.Formats.Alembic.Importer;

namespace VRGame
{
	[ExecuteInEditMode]
	public class MakeCollidersForAlembicFile : EditorScript
	{
        public float makeInterval;
		public AlembicStreamPlayer alembicStreamPlayer;
		public GameObject[] gos = new GameObject[0];
		public _Animation2.ColliderEntry[] colliderEntries = new _Animation2.ColliderEntry[0];

		public override void Do ()
		{
			if (alembicStreamPlayer == null)
				alembicStreamPlayer = GetComponent<AlembicStreamPlayer>();
			_Do (alembicStreamPlayer, makeInterval, gos);
			EditorApplication.update += () => { SetColliderEntries (ref colliderEntries, gos, alembicStreamPlayer); };
		}

		static void _Do (AlembicStreamPlayer alembicStreamPlayer, float makeInterval, GameObject[] gos)
		{
			for (int i = 0; i < gos.Length; i ++)
			{
				GameObject go = gos[i];
				MeshCollider[] meshColliders = go.GetComponents<MeshCollider>();
				for (int i2 = 0; i2 < meshColliders.Length; i2 ++)
				{
					MeshCollider meshCollider = meshColliders[i2];
					GameManager.DestroyOnNextEditorUpdate (meshCollider);
				}
			}
			for (float time = 0; time < alembicStreamPlayer.EndTime; time += makeInterval)
			{
				alembicStreamPlayer.UpdateImmediately(time);
				for (int i = 0; i < gos.Length; i ++)
				{
					GameObject go = gos[i];
					MeshCollider meshCollider = go.AddComponent<MeshCollider>();
					meshCollider.sharedMesh = Instantiate(go.GetComponent<MeshFilter>().sharedMesh);
					meshCollider.enabled = false;
				}
			}
		}

		static void SetColliderEntries (ref _Animation2.ColliderEntry[] colliderEntries, GameObject[] gos, AlembicStreamPlayer alembicStreamPlayer)
		{
			colliderEntries = new _Animation2.ColliderEntry[gos.Length];
			for (int i = 0; i < gos.Length; i ++)
			{
				GameObject go = gos[i];
				colliderEntries[i] = new _Animation2.ColliderEntry(go, alembicStreamPlayer.EndTime);
			}
			EditorApplication.update = null;
		}
	}
}
#else
namespace VRGame
{
	public class MakeCollidersForAlembicFile : EditorScript
	{
	}
}
#endif