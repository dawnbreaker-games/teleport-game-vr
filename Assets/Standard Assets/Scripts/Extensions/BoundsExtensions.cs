﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Extensions
{
	public static class BoundsExtensions
	{
		public static Bounds NULL = new Bounds(VectorExtensions.NULL3, VectorExtensions.NULL3);

		public static bool IsEncapsulating (Bounds b1, Bounds b2, bool equalBoundsRetunsTrue)
		{
			if (equalBoundsRetunsTrue)
			{
				bool minIsOk = b1.min.x <= b2.min.x && b1.min.y <= b2.min.y && b1.min.z <= b2.min.z;
				bool maxIsOk = b1.min.x >= b2.min.x && b1.min.y >= b2.min.y && b1.min.z >= b2.min.z;
				return minIsOk && maxIsOk;
			}
			else
			{
				bool minIsOk = b1.min.x < b2.min.x && b1.min.y < b2.min.y && b1.min.z < b2.min.z;
				bool maxIsOk = b1.max.x > b2.max.x && b1.max.y > b2.max.y && b1.max.z > b2.max.z;
				return minIsOk && maxIsOk;
			}
		}

		
		public static bool Intersects (Bounds b1, Bounds b2, Vector3 expandB1 = new Vector3(), Vector3 expandB2 = new Vector3())
		{
			b1.Expand(expandB1);
			b2.Expand(expandB2);
			return b1.Intersects(b2);
		}

		public static Vector3 RandomPoint (this Bounds bounds)
		{
			return new Vector3(Random.Range(bounds.min.x, bounds.max.x), Random.Range(bounds.min.y, bounds.max.y), Random.Range(bounds.min.z, bounds.max.z));
		}
	}
}